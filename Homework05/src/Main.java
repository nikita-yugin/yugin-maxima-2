import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Укажите размер массива: ");
        int size = scanner.nextInt();
        int[] ages = new int[size];

        System.out.println("Введите значения массива в диапазоне от 0 до 120: ");
        for (int i = 0; i < ages.length; i++) {
            ages[i] = scanner.nextInt();
        }

        System.out.println(Arrays.toString(ages));

        int popularAge = 0;
        for (int i = 0; i < ages.length; i++) {
            for (int j = i + 1; j < ages.length; j++) {
                if (ages[i] == ages[j]) {
                    popularAge = ages[i];
                }
            }
        }

        System.out.println("Наиболее встречающийся возраст: " + popularAge);
    }
}
