public class Main {
    public static void main(String[] args) throws Exception {
        TextConverter textConverter = new TextConverter();

        textConverter.toLowerCaseAll("input.txt", "output.txt");
    }
}
