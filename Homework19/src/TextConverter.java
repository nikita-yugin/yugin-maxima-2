import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TextConverter {

    public void toLowerCaseAll(String sourceFileName, String targetFileName) throws Exception {
        int[] bytes = getInputFile(sourceFileName);

        char[] characters = new char[30];

        for (int i = 0; i < bytes.length; i++) {
            characters[i] = (char) bytes[i];
        }

        for (int i = 0; i < characters.length; i++) {
            if (!Character.isLetter(characters[i])) {
                characters[i] = ' ';
            }
        }

        String message = new String(characters).toLowerCase();

        outputFile(targetFileName, message);
    }

    private int[] getInputFile(String sourceFileName) throws IOException {
        FileInputStream inputStream = new FileInputStream(sourceFileName);

        int[] bytes = new int[30];
        int current = inputStream.read();

        int position = 0;
        while (current != -1) {
            bytes[position] = current;
            position++;

            current = inputStream.read();
        }

        inputStream.close();
        return bytes;
    }

    private void outputFile(String targetFileName, String message) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(targetFileName);
        byte[] newBytes = message.getBytes();
        outputStream.write(newBytes);
        outputStream.close();
    }

}
