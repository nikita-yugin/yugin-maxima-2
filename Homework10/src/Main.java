import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Logger logger = Logger.getInstance();
        Logger logger2 = Logger.getInstance();

        logger.addMessages("Привет Земляне!");
        logger.addMessages("Новая Земля");
        logger.addMessages("Maxima IT School");

        logger2.addMessages("Имена всех членов класса");
        logger2.addMessages("Кому как");
        logger2.addMessages("Интересно, но факт");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            String message = scanner.nextLine();
            logger.log(message);
        }

    }
}
