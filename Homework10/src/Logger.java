public class Logger {
    private final static Logger instance;
    private final int MAX_MESSAGES_COUNT = 15;
    private String[] messages;
    private int count;

    static {
        instance = new Logger();
    }

    private Logger() {
        messages = new String[MAX_MESSAGES_COUNT];
    }

    public static Logger getInstance() {
        return instance;
    }

    public void addMessages(String message) {
        this.messages[count] = message;
        count++;
    }

    public void log(String message) {
        for (int i = 0; i < count; i++) {
            if (messages[i].startsWith(message)) {
                System.out.println(messages[i]);
            }
        }
    }
}
