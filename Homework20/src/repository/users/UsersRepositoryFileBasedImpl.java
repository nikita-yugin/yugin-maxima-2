package repository.users;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryFileBasedImpl {
    private String fileName;

    public List<String> findAll() {
        List<String> listUsers = new ArrayList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            while (reader.ready()) {
                String text = reader.readLine();
                listUsers.add(text);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                reader.close();
            } catch (IOException ignore) {}
        }

        return listUsers;
    }

    public Optional<User> findByFirstName(String firstName) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            String userName = reader.readLine();

            while (userName != null) {
                String[] parseUser = userName.split("\\|");

                if (parseUser[0].equals(firstName)) {
                    String searchFirstName = parseUser[0];
                    String searchLastName = parseUser[1];
                    User user = new User(searchFirstName, searchLastName);
                    return Optional.of(user);
                }

                userName = reader.readLine();
            }

            return Optional.empty();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public UsersRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    public void save(User user) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write(user.getFirstName() + "|" + user.getLastName());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                writer.close();
            } catch (IOException ignore) {}
        }
    }
}
