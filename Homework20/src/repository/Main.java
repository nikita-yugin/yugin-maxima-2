package repository;

import repository.users.User;
import repository.users.UsersRepositoryFileBasedImpl;

public class Main {
    public static void main(String[] args) {
        UsersRepositoryFileBasedImpl repositoryUsers = new UsersRepositoryFileBasedImpl("users.txt");

        User nikita = new User("Никита", "Югин");
        User diana = new User("Диана", "Турсунова");
        User maxim = new User("Максим", "Исаев");

        repositoryUsers.save(nikita);
        repositoryUsers.save(diana);
        repositoryUsers.save(maxim);

        System.out.println(repositoryUsers.findAll());

        System.out.println(repositoryUsers.findByFirstName("Диана"));
    }
}
