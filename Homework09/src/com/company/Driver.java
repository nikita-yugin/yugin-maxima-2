package com.company;

public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    // конструктор в котором мы передаем имя и опыт водителя
    public Driver(String name, int experience) {
        this.name = name;
        // если введённый опыт больше нуля
        if (experience > 0) {
            // присваиваем его к нашему полю "опыт"
            this.experience = experience;
        } else {
            // иначе выводим предупреждение:
            System.err.println("ОПЫТА У " + name + " НЕТ!");
        }
    }

    // сеттер
    public void setBus(Bus bus) {
        // закрепляем автобус за водителем
        this.bus = bus;
    }

    public void drive(Bus bus) {
        if (bus.getState() == 0) {
            bus.setState(1);
            System.out.println("Водитель завёл автобус");

            for (int i = 0; i < bus.getPassengers().length; i++) {
                bus.getPassengers()[i].sayName();
            }
        } else {
            System.err.println("Автобус уже заведён");
        }
    }
}
