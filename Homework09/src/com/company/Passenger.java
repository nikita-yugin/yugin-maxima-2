package com.company;

public class Passenger {
    private String name;
    // объявляем объект, поле, которое ссылается на какой-то автобус
    private Bus bus;
    private Driver driver;

    // перегруженные конструкторы
    // конструктор без параметров
    public Passenger() {
        this.name = "Без имени";
    }

    // конструктор с параметром
    public Passenger(String name) {
        this.name = name;
    }

    // геттер на имя
    public String getName() {
        // возвращаем имя
        return name;
    }

    // метод в котором сажаем пассажира в автобус
    public void goToBus(Bus bus) {
        // проверяем не сидим ли мы уже в автобусе
        if (this.bus != null) {
            // если мы уже в автобусе, выводим предупреждение:
            System.err.println("Я, " + name + ", уже в автобусе:)");
        } else {
            // если мы не сидим в автобусе
            // то проверяем не полный ли автобус
            if (!bus.isFull()) {
                // начинаем ссылаться на этот автобус
                this.bus = bus;
                // передаем автобусу сами себя
                this.bus.incomePassenger(this);
            } else {
                // если автобус полный, выводим предупреждение:
                System.err.println("Я, " + name + ", не попал в автобус(");
            }
        }
    }

    public void leaveBus() {
        if (bus.getState() == 1) {
            System.err.println("Покидать свои места запрещено! Автобус находится в движении!");
        }
    }

    public void sayName() {
        System.out.println("Я " + this.name);
    }
}
