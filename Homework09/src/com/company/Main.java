package com.company;

public class Main {

    public static void main(String[] args) {
        Passenger passenger1 = new Passenger("Alex");
        Passenger passenger2 = new Passenger("Nik");
        Passenger passenger3 = new Passenger("Di");
        Passenger passenger4 = new Passenger("Richard");
        Passenger passenger5 = new Passenger("Felix");
        Passenger passenger6 = new Passenger("Jess");
        Passenger passenger7 = new Passenger("Keil");

        Driver driver = new Driver("Maxim", 10);
        Driver driver1 = new Driver(" Tom", 5);

        Bus bus = new Bus("LiAZ", 7, 5);

        bus.setDriver(driver);
        driver.setBus(bus);

        passenger1.goToBus(bus);
        passenger2.goToBus(bus);
        passenger3.goToBus(bus);
        passenger7.goToBus(bus);

        driver.drive(bus);

        bus.setDriver(driver1);

        int i = 0;
    }
}
