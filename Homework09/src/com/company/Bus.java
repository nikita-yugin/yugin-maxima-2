package com.company;

public class Bus {
    private boolean isGoing;
    private String model;
    private int number;
    private Driver driver;
    // массив пассажиров
    private Passenger[] passengers;
    // кол-во пассажиров в автобусе на данный момент
    private int count;
    private int state;


    // конструктор в котором передаем:
    // модель автобуса, маршрут и сколько пассажиров может взять на борт автобус
    public Bus(String model, int number, int placeCount) {
        this.model = model;
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        // создали placeCount объектныъ переменных в которые можно положить пассажира
        this.passengers = new Passenger[placeCount];
    }

    // метод в котором автобус принимаем пассажира
    public void incomePassenger(Passenger passenger) {
        if (this.state == 1) {
            System.err.println("Автобус находится в пути, посадка пассажиров невозможна!");
        } else {
            if (!this.isGoing) {
                // проверяем не полный ли автобус
                if (this.count < this.passengers.length) {
                    // если места есть, то передаём пассажира автобусу
                    this.passengers[count] = passenger;
                    // увеличиваем фактическое кол-во пассажиров
                    this.count++;
                } else {
                    // если автобус полный, выодим предупреждение:
                    System.err.println("Автобус переполнен!");
                }
            } else {
                System.err.println("автобус в пути");
            }
        }
    }

    // сеттер
    public void setDriver(Driver driver) {
        if (this.state == 1) {
            System.err.println("Мы не можем поменять водителя так как автобус в движении!");
        } else {
            if (!this.isGoing) {
                // закрепляем автобус за водителем
                this.driver = driver;
                // закрепляем водителя за автобусом
                driver.setBus(this);
            } else {
                System.err.println("Автобус в движении водителя менять запрещено!");
            }
        }
    }

    // метод проверяющий не полный ли автобус
    public boolean isFull() {
        return this.count == this.passengers.length;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public Passenger[] getPassengers() {
        return passengers;
    }
}
