import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {

    //language=SQL
    public static final String SQL_SELECT_FROM_FOOD_PRODUCTS = "select * from food_products order by id";

    public static void main(String[] args) {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_FOOD_PRODUCTS)){
                while (resultSet.next()) {
                    String productName = resultSet.getString("product_name");
                    int price = resultSet.getInt("price");
                    String deliveryDate = resultSet.getString("delivery_date");
                    String supplierName = resultSet.getString("supplier_name");

                    System.out.println(productName + " " + price + " " + deliveryDate + " " + supplierName);
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}