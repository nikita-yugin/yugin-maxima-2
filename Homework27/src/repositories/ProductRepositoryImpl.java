package repositories;

import models.Product;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ProductRepositoryImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_INSERT_INTO_PRODUCT = " insert into food_products(product_name, price, self_life, delivery_date, supplier_name) " +
            "values (?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_FIND_ALL_BY_PRICE = "select * from food_products where price = ?;";

    //language=SQL
    private static final String SQL_FIND_ONE_BY_NAME = "select * from food_products where product_name = ?;";

    //language=SQL
    private static final String SQL_UPDATE_FOOD_PRODUCTS = "insert into food_products (product_name, price, self_life, delivery_date, supplier_name) " +
            "values (?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_DELETE_COLUMN_FOOD_PRODUCTS = "delete from food_products where " +
            "id = ?";

    private final JdbcTemplate jdbcTemplate;

    public ProductRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (RowMapper<Product>) (row, rowNumber) -> new Product(
            row.getLong("id"),
            row.getString("product_name"),
            row.getDouble("price"),
            row.getInt("self_life"),
            row.getDate("delivery_date"),
            row.getString("supplier_name"));

    @Override
    public void save(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_PRODUCT, Statement.RETURN_GENERATED_KEYS);

                statement.setString(1, product.getProductName());
                statement.setDouble(2, product.getPrice());
                statement.setInt(3, product.getSelfLife());
                statement.setDate(4, product.getDeliveryDate());
                statement.setString(5, product.getSupplierName());

                return statement;
            }
        }, keyHolder);

        Long generatedId = ((Integer) keyHolder.getKeys().get("id")).longValue();
        product.setId(generatedId);
    }

    @Override
    public List<Map<String, Object>> findAllByPrice(double price) {

        List<Map<String, Object>> productList = new ArrayList<>();

        return productList = jdbcTemplate.queryForList(SQL_FIND_ALL_BY_PRICE, price);
    }

    @Override
    public Optional<Product> findOneByName(String name) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_FIND_ONE_BY_NAME, productRowMapper, name));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        jdbcTemplate.update(
                SQL_UPDATE_FOOD_PRODUCTS,
                product.getProductName(),
                product.getPrice(),
                product.getSelfLife(),
                product.getDeliveryDate(),
                product.getSupplierName());
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update(SQL_DELETE_COLUMN_FOOD_PRODUCTS, id);
    }
}
