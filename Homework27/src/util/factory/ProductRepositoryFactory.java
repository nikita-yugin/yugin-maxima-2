package util.factory;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import repositories.ProductRepository;
import repositories.ProductRepositoryImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ProductRepositoryFactory {
    public static ProductRepository onJdbc() {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new DriverManagerDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));

        return new ProductRepositoryImpl(dataSource);
    }
}
