import models.Product;
import repositories.ProductRepository;
import util.factory.ProductRepositoryFactory;

import java.sql.Date;

public class Main {
    public static void main(String[] args) {
        ProductRepository repository = ProductRepositoryFactory.onJdbc();

        repository.delete(11L);
    }
}