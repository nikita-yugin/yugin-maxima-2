public class ArrayList {
    private final static int DEFAULT_SIZE = 10;
    private int[] elements;
    private int size;

    public ArrayList() {
        this.elements = new int[DEFAULT_SIZE];
    }

    public void add(int element) {
        ensureSize();

        this.elements[size++] = element;
    }


    public void add(int index, int element) {
        this.elements[index] = element;
    }

    public void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }

    private void resize() {
        int[] newArray = new int[this.elements.length + elements.length / 2];
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    public int get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.err.println("Под этим индексом ничего нет");
            return -1;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public int size() {
        return size;
    }

    public void clear() {
        size = 0;
    }

    /**
     * Удаляет элемент в заданном индексе, смещая элементы, которые идут после удаляемого на одну позицию влево
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        for (int i = index; i < size; i++) {
            elements[i] = elements[i + 1];
        }
        size--;
    }

    /**
     * Удаляет первое вхождение элемента в список
     * @param element удаляемый элемент списка
     */
    public void remove(int element) {
        for (int i = 0; i < size; i++) {
            if (element == elements[i]) {
                removeAt(i);
                return;
            }
        }
    }

    /**
     * Удаляет последнее вхождение элемента в список
     * @param element удаляемый элемент списка
     */
    public void removeLast(int element) {
        for (int i = size; i >= 0; i--) {
            if (element == elements[i]) {
                removeAt(i);
                return;
            }
        }
    }

    /**
     * Удаляет все вхожденя данного элемента в список
     * @param element удаляемый элемент списка
     */
    public void removeAll(int element) {
        for (int i = 0; i < size - 1; i++) {
            if (element == elements[i]) {
                for (int j = i; j < size; j++) {
                    elements[j] = elements[j + 1];
                }
                size--;
            }
        }
    }
}
