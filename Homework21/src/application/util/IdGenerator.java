package application.util;

public interface IdGenerator {

    Long generate();
}
