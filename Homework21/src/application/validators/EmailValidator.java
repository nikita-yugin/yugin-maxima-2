package application.validators;

import application.validators.exceptions.EmailValidatorException;

public interface EmailValidator {
    void validate(String email) throws EmailValidatorException;
}
