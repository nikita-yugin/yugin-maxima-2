package application.validators;

import application.validators.exceptions.EmailValidatorException;

public class EmailFormatValidator implements EmailValidator {
    @Override
    public void validate(String email) throws EmailValidatorException {
        if (!email.contains("@")) {
            throw new EmailValidatorException();
        }
    }
}
