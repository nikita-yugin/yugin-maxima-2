package application.validators;

import application.validators.exceptions.PasswordValidatorException;

public class PasswordLengthValidator implements PasswordValidator {
    @Override
    public void validate(String password) throws PasswordValidatorException {
        if (password.length() < 5) {
            throw new PasswordValidatorException();
        }
    }
}
