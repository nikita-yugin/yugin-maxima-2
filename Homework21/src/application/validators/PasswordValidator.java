package application.validators;

import application.validators.exceptions.PasswordValidatorException;

public interface PasswordValidator {
    void validate(String password) throws PasswordValidatorException;
}
