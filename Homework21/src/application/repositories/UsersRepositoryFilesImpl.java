package application.repositories;

import application.models.User;
import application.repositories.exceptions.UniqueEmailException;
import application.util.IdGenerator;

import java.io.*;
import java.util.Optional;

public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;
    private final IdGenerator generator;

    public UsersRepositoryFilesImpl(String fileName, IdGenerator generator) {
        this.fileName = fileName;
        this.generator = generator;
    }

    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            checkUniqueEmail(user.getEmail());
            user.setId(generator.generate());
            writer.write(user.getId() + "|" +
                    user.getEmail() + "|" +
                    user.getPassword());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String userLine = reader.readLine();

            while (userLine != null) {
                String[] splitUserLine = userLine.split("\\|");

                if (splitUserLine[1].equals(email)) {
                    String idSplit = splitUserLine[0];
                    String emailSplit = splitUserLine[1];
                    String passwordSplit = splitUserLine[2];

                    User user = new User(Long.parseLong(idSplit), emailSplit, passwordSplit);
                    return Optional.of(user);
                }

                userLine = reader.readLine();
            }

            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    // Проверка уникальности Email-a
    private void checkUniqueEmail(String email) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            String userLine = reader.readLine();

            while (userLine != null) {
                String[] userSplit = userLine.split("\\|");

                if (userSplit[1].equals(email)) {
                    throw new UniqueEmailException();
                }

                userLine = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
