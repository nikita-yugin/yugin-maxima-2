package application.repositories;

import application.models.User;
import application.repositories.exceptions.UniqueEmailException;

import java.util.Optional;

public interface UsersRepository {

    void save(User user) throws UniqueEmailException;

    Optional<User> findByEmail(String email);
}
