package application;

import application.models.User;
import application.repositories.UsersRepository;
import application.repositories.UsersRepositoryFilesImpl;
import application.services.UsersService;
import application.services.UsersServiceImpl;
import application.util.IdGenerator;
import application.util.IdGeneratorFilesImpl;
import application.validators.EmailFormatValidator;
import application.validators.EmailValidator;
import application.validators.PasswordLengthValidator;
import application.validators.PasswordValidator;

public class Main {
    public static void main(String[] args) {
        IdGenerator generator = new IdGeneratorFilesImpl("users_id_sequence.txt");
        UsersRepository repository = new UsersRepositoryFilesImpl("users.txt", generator);
        EmailValidator emailValidator = new EmailFormatValidator();
        PasswordValidator passwordValidator = new PasswordLengthValidator();
        UsersService service = new UsersServiceImpl(repository, emailValidator, passwordValidator);
    }
}