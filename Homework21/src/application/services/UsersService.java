package application.services;

import application.services.exceptions.AuthenticationException;

public interface UsersService {
    void signUp(String email, String password);

    void signIn(String email, String password) throws AuthenticationException;
}
