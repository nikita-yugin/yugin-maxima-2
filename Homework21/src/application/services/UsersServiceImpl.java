package application.services;

import application.models.User;
import application.repositories.UsersRepository;
import application.services.exceptions.AuthenticationException;
import application.validators.EmailValidator;
import application.validators.PasswordValidator;

import java.util.Optional;

public class UsersServiceImpl implements UsersService {

    private final UsersRepository repository;
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;

    public UsersServiceImpl(UsersRepository repository,
                            EmailValidator emailValidator,
                            PasswordValidator passwordValidator) {
        this.repository = repository;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }

    @Override
    public void signUp(String email, String password) {
        emailValidator.validate(email);
        passwordValidator.validate(password);

        User user = new User(email, password);
        repository.save(user);
    }

    @Override
    public void signIn(String email, String password) throws AuthenticationException {
        Optional<User> userOptional = repository.findByEmail(email);

        if (userOptional.isPresent()) {
            User user = userOptional.get();

            if (!user.getPassword().equals(password)) {
                throw new AuthenticationException();
            }

            return;
        }

        throw new AuthenticationException();
    }
}
