package models;

import java.sql.Date;
import java.util.Objects;

public class Product {
    private long id;
    private final String productName;
    private final double price;
    private final int selfLife;
    private final Date deliveryDate;
    private final String supplierName;

    public Product(long id, String productName,
                   double price,
                   int selfLife,
                   Date deliveryDate,
                   String supplierName) {
        this.id = id;
        this.productName = productName;
        this.price = price;
        this.selfLife = selfLife;
        this.deliveryDate = deliveryDate;
        this.supplierName = supplierName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public double getPrice() {
        return price;
    }

    public int getSelfLife() {
        return selfLife;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public String getSupplierName() {
        return supplierName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id && price == product.price && selfLife == product.selfLife && Objects.equals(productName, product.productName) && Objects.equals(deliveryDate, product.deliveryDate) && Objects.equals(supplierName, product.supplierName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productName, price, selfLife, deliveryDate, supplierName);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", selfLife=" + selfLife +
                ", deliveryDate='" + deliveryDate + '\'' +
                ", supplierName='" + supplierName + '\'' +
                '}';
    }
}
