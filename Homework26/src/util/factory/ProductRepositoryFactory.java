package util.factory;

import repositories.ProductRepository;
import repositories.ProductRepositoryImpl;
import util.jdbc.DataSourceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ProductRepositoryFactory {
    public static ProductRepository onJdbc() {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new DataSourceImpl(properties);

        return new ProductRepositoryImpl(dataSource);
    }
}
