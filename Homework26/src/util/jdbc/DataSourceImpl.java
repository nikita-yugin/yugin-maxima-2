package util.jdbc;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class DataSourceImpl implements DataSource {

    private final Properties properties;

    private Connection connection;

    public DataSourceImpl(Properties properties) {
        this.properties = properties;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(
                    properties.getProperty("db.url"),
                    properties.getProperty("db.user"),
                    properties.getProperty("db.password"));
        }

        return this.connection;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        throw new IllegalArgumentException("Method not implemented");
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        throw new IllegalArgumentException("Method not implemented");
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        throw new IllegalArgumentException("Method not implemented");
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        throw new IllegalArgumentException("Method not implemented");
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        throw new IllegalArgumentException("Method not implemented");
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new IllegalArgumentException("Method not implemented");
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        throw new IllegalArgumentException("Method not implemented");
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        throw new IllegalArgumentException("Method not implemented");
    }
}
