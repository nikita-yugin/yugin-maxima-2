import models.Product;
import repositories.ProductRepository;
import util.factory.ProductRepositoryFactory;

import java.sql.Date;

public class Main {
    public static void main(String[] args) {
        ProductRepository repository = ProductRepositoryFactory.onJdbc();
        Product product = new Product(0, "BMW", 400000, 3, Date.valueOf("2020-08-11"), "BMW RUS");
        repository.findOneByName("BMW");
    }
}