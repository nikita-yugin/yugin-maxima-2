package repositories;

import models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {
    void save(Product product);

    List<Product> findAllByPrice(double price);

    Optional<Product> findOneByName(String name);

    void update(Product product);

    void delete(Product product);
}
