package repositories;

import models.Product;
import util.jdbc.RowMapper;

import javax.sql.DataSource;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductRepositoryImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_INSERT_INTO_PRODUCT = " insert into food_products(product_name, price, self_life, delivery_date, supplier_name) " +
            "values (?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_FIND_ALL_BY_PRICE = "select * from food_products where price = ?;";

    //language=SQL
    private static final String SQL_FIND_ONE_BY_NAME = "select * from food_products where product_name = ?;";

    //language=SQL
    private static final String SQL_UPDATE_FOOD_PRODUCTS = "insert into food_products (product_name, price, self_life, delivery_date, supplier_name) " +
            "values (?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_DELETE_COLUMN_FOOD_PRODUCTS = "delete from food_products where " +
            "product_name = ? " +
            "and price = ? " +
            "and delivery_date = ? " +
            "and supplier_name = ?;";

    private final DataSource dataSource;

    public ProductRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final RowMapper<Product> productRowMapper = (RowMapper<Product>) row -> new Product(
            row.getLong("id"),
            row.getString("product_name"),
            row.getDouble("price"),
            row.getInt("self_life"),
            row.getDate("delivery_date"),
            row.getString("supplier_name"));

    @Override
    public void save(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     SQL_INSERT_INTO_PRODUCT,
                     Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, product.getProductName());
            statement.setDouble(2, product.getPrice());
            statement.setInt(3, product.getSelfLife());
            statement.setDate(4, product.getDeliveryDate());
            statement.setString(5, product.getSupplierName());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't save product");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (!generatedKeys.next()) {
                throw new SQLException("Can't retrieve generated id");
            }

            Long generatedId = generatedKeys.getLong("id");

            product.setId(generatedId);

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Product> findAllByPrice(double price) {

        List<Product> productList = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_BY_PRICE)) {
            statement.setDouble(1, price);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    Product product = productRowMapper.mapRow(resultSet);

                    productList.add(product);
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return productList;
    }

    @Override
    public Optional<Product> findOneByName(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_ONE_BY_NAME)) {
            statement.setString(1, name);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Product product = productRowMapper.mapRow(resultSet);

                    if (resultSet.next()) {
                        throw new SQLException("More than one rows");
                    }

                    return Optional.of(product);

                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_FOOD_PRODUCTS)) {

            statement.setString(1, product.getProductName());
            statement.setDouble(2, product.getPrice());
            statement.setInt(3, product.getSelfLife());
            statement.setDate(4, product.getDeliveryDate());
            statement.setString(5, product.getSupplierName());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert product");
            } else {
                System.out.println("Insert completed");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_COLUMN_FOOD_PRODUCTS)) {
            statement.setString(1, product.getProductName());
            statement.setDouble(2, product.getPrice());
            statement.setDate(3, product.getDeliveryDate());
            statement.setString(4, product.getSupplierName());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't delete product");
            } else {
                System.out.println("Product delete");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
