public class MapOfEntriesImpl<K, V> implements Map<K, V> {

    private static final int MAX_SIZE_COUNT = 10;

    private Entry<K, V>[] entries;
    private int count;

    public MapOfEntriesImpl() {
        this.entries = new Entry[MAX_SIZE_COUNT];
    }

    @Override
    public void put(K key, V value) {
        int hash = key.hashCode();
        int index = Math.abs(hash % 10);
        entries[index] = new Entry<>(key, value);
        count++;
    }

    private static class Entry<K, V> {
        K key;
        V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
