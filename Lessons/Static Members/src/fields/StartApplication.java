package fields;

import java.time.LocalTime;

public class StartApplication {
    public static LocalTime startTime;
    public LocalTime objectCreatedTime;

    static {
        startTime = LocalTime.now();
    }

    public StartApplication() {
        objectCreatedTime = LocalTime.now();
    }

    public static void printStartTime() {
        System.out.println(startTime.getHour() + ":" + startTime.getMinute() + ":" + startTime.getSecond());
    }

    public void printObjectCreatedTime() {
        System.out.println(objectCreatedTime.getHour() + ":" + objectCreatedTime.getMinute() + ":" + objectCreatedTime.getSecond());
    }
}
