package fields;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    StartApplication application1 = new StartApplication();
	    Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        StartApplication application2 = new StartApplication();
        application1.printObjectCreatedTime();
        application2.printObjectCreatedTime();
        StartApplication.printStartTime();
    }
}
