package singletone;

public class Dictionary {
    private final static Dictionary instance;
    private final int MAX_WORDS_COUNT = 15;
    private String[] words;
    private int count;

    static {
        instance = new Dictionary();
    }

    private Dictionary() {
        words = new String[MAX_WORDS_COUNT];
    }

    public static Dictionary getInstance() {
        return instance;
    }

    public void addWord(String word) {
        this.words[count] = word;
        count++;
    }

    public String complete(String prefix) {
        for (int i = 0; i < count; i++) {
            if (words[i].startsWith(prefix)) {
                return words[i];
            }
        }
        return "";
    }
}
