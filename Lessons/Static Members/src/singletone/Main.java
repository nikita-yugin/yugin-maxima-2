package singletone;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Dictionary dictionary = Dictionary.getInstance();
        Dictionary dictionary1 = Dictionary.getInstance();

        dictionary1.addWord("Java");
        dictionary1.addWord("Singleton");
        dictionary1.addWord("C++");

        dictionary.addWord("Привет");
        dictionary.addWord("Пока");
        dictionary.addWord("Что?");
        dictionary.addWord("Как");
        dictionary.addWord("Марсель");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            String word = scanner.nextLine();
            System.out.println(dictionary1.complete(word));
        }

    }
}
