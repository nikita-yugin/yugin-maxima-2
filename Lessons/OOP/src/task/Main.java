package task;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();
        Human[] humans = new Human[100];
        int[] ages = new int[120];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].name = "User" + i;
            humans[i].setAge(random.nextInt(120));
        }

        for (int i = 0; i < humans.length; i++) {
            int currentAge = humans[i].getAge();
            ages[currentAge]++;
        }

        for (int i = 0; i < humans.length; i++) {
            System.out.println(humans[i].name + " " + humans[i].getAge());
        }

        System.out.println("=================");

        for (int i = 0; i < ages.length; i++) {
            System.out.println("Людей с возрастом " + i + " - " + ages[i] + " штук");
        }

        System.out.println("=================");

        int value = 0;
        int mostPopularAge = 0;
        for (int i = 0; i < ages.length; i++) {
            if (value < ages[i]) {
                mostPopularAge = i;
                value = ages[i];
            }
        }

        System.out.println("Самый популярный возраст " + mostPopularAge + " лет, людей этого возраста " + value + " штук");
    }
}
