package task;

public class Human {
    String name;
    private int age;

    int getAge() {
        return age;
    }

    void setAge(int age) {
        if (age >= 0 && age < 120) {
            this.age = age;
        } else {
            age = 0;
        }
    }
}
