package collections;

import javax.security.auth.kerberos.KerberosKey;

public interface Map<K, V> {
    void put(K key, V value);

    V get(K key);

    boolean containsKey(K key);

    Set<K> keySet();

    interface MapEntry<K, V> {
        K getKey();

        V getValue();
    }

    Set<Map.MapEntry<K, V>> entrySet();
}
