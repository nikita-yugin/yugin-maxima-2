package collections;

import java.util.Map.Entry;

public class MapHashImpl<K, V> implements Map<K, V> {
    private final static int MAP_SIZE = 16;

    private Entry<K, V>[] entries;
    private int count;

    private static class Entry<K, V> implements Map.MapEntry<K, V> {
        int hash;
        K key;
        V value;
        Entry<K, V> next;

        Entry(K key, V value, int hash) {
            this.key = key;
            this.value = value;
            this.hash = hash;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }
    }

    public MapHashImpl() {
        this.entries = new Entry[MAP_SIZE];
        this.count = 0;
    }

    @Override
    public void put(K key, V value) {
        int hash = key.hashCode();
        int index = hash & (MAP_SIZE - 1);

        if (entries[index] != null) {
            Entry<K, V> current = entries[index];

            if (current.hash == hash && current.key.equals(key)) {
                current.value = value;
                return;
            }

            while (current.next != null) {
                if (current.hash == hash && current.key.equals(key)) {
                    current.value = value;
                    return;
                }

                current = current.next;
            }

            current.next = new Entry<>(key, value, hash);
            count++;
        } else {
            entries[index] = new Entry<>(key, value, hash);
            count++;
        }
    }

    @Override
    public V get(K key) {
        int hash = key.hashCode();
        int index = hash & (MAP_SIZE -1);

        if (entries[index] != null) {
            Entry<K, V> current = entries[index];

            while (current != null) {
                if (current.hash == hash && current.key.equals(key)) {
                    return current.value;
                }

                current = current.next;
            }

            return null;
        } else {
            return null;
        }
    }

    @Override
    public boolean containsKey(K key) {
        int hash = key.hashCode();
        int index = hash & (MAP_SIZE - 1);

        if (entries[index] != null) {
            Entry<K, V> current = entries[index];

            while (current != null) {
                if (current.hash == hash && current.key.equals(key)) {
                    return true;
                }

                current = current.next;
            }
        }

        return false;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new SetHashImpl<>();

        for (int i = 0; i < entries.length; i++)  {
            if (entries[i] != null) {
                Entry<K, V> current = entries[i];

                while (current != null) {
                    keySet.add(current.key);
                    current = current.next;
                }
            }
        }

        return keySet;
    }

    @Override
    public Set<Map.MapEntry<K, V>> entrySet() {
        Set<Map.MapEntry<K, V>> entrySet = new SetHashImpl<>();

        for (int i = 0; i < entries.length; i++) {
            if (entries[i] != null) {
                Entry<K, V> current = entries[i];

                while (current != null) {
                    entrySet.add(current);
                    current = current.next;
                }
            }
        }

        return entrySet;
    }


}
