public class Main {

    public static int hash1(String string) {
        char[] characters = string.toCharArray();
        int hash = 0;
        for (int i = 0; i < characters.length; i++) {
            hash += characters[i];
        }
        return hash;
    }

    public static int hash2(String string) {
        char[] characters = string.toCharArray();
        int hash = 0;
        for (int i = 0; i < characters.length; i++) {
            hash += characters[i] * i;
        }
        return hash;
    }

    public static int hash3(String string) {
        int hash = 0;

        char[] characters = string.toCharArray();

        for (int i = 0; i < characters.length; i++) {
            hash = 31 * hash + characters[i];
        }
        return hash;
    }

    public static void main(String[] args) {
        System.out.println(hash3("Марсель"));
        System.out.println(hash3("Виктор"));
        System.out.println(hash3("Никита"));
    }
}
