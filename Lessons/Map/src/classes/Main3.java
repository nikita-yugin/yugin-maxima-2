package classes;

import collections.Map;
import collections.MapHashImpl;

public class Main3 {
    public static void main(String[] args) {
        Human nikita = new Human(1, "Nikita", "Yugin", 10);
        Human secondNikita = new Human(1, "Nikita", "Yugin", 20);

        Human diana = new Human(2, "Diana", "Tursunova", 10);
        Human secondDiana = new Human(2, "Diana", "Tursunova", 15);

        Human marsel = new Human(3, "Marsel", "Sidikov", 15);

        System.out.println(nikita.equals(diana));
        System.out.println(nikita.equals(marsel));
        System.out.println(nikita.equals(secondNikita));
        System.out.println(diana.equals(secondDiana));

        System.out.println(nikita.hashCode());
        System.out.println(diana.hashCode());
        System.out.println(marsel.hashCode());

        Map<Human, String> humans = new MapHashImpl<>();
        humans.put(nikita, "Привет!");
        humans.put(diana, "Как дела?");
        humans.put(marsel, "Где домашка?");

        System.out.println(humans.get(nikita));
        System.out.println(humans.get(secondNikita));
        System.out.println(humans.get(diana));
        System.out.println(humans.get(secondDiana));
        System.out.println(humans.get(marsel));

        System.out.println(nikita.hashCode());
        System.out.println(secondNikita.hashCode());
        System.out.println(diana.hashCode());
        System.out.println(secondDiana.hashCode());
    }
}
