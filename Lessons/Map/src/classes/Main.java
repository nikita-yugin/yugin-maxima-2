package classes;

import collections.Map;
import collections.MapHashImpl;

public class Main {

    public static boolean isOnXY(String name, int x, int y, Map<Point, String> surface) {
        Point point = new Point(x, y);
        String existedHuman = surface.get(point);

        return existedHuman != null && existedHuman.equals(name);
    }

    public static void main(String[] args) {
        Map<Point, String> surface = new MapHashImpl<>();

        Point a = new Point(10, 10);
        Point b = new Point(15, 15);
        Point c = new Point(20, 20);
        Point d = new Point(25, 25);

        surface.put(a, "Марсель");
        surface.put(b, "Разиль");
        surface.put(c, "Айрат");
        surface.put(d, "Максим");

        System.out.println(isOnXY("Айрат", 20, 20, surface));
        System.out.println(isOnXY("Максим", 20, 20, surface));
        System.out.println(isOnXY("Максим", 30, 30, surface));
    }
}
