package classes;

public class Human {
    private int id;
    private String firstName;
    private String lastName;

    private int friendsCount;

    public Human(int id, String firstName, String lastName, int friendsCount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.friendsCount = friendsCount;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj instanceof Human) {
            Human that = (Human) obj;

            return this.id == that.id
                    && this.firstName.equals(that.firstName)
                    && this.lastName.equals(that.lastName);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int idHash = id;
        int firstNameHash = firstName.hashCode();
        int lastNameHash = lastName.hashCode();

        return idHash + firstNameHash * 31 + lastNameHash * 31 * 31;
    }
}
