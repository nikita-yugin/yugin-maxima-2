//package old;
//
//import java.util.Map.Entry;
//
//public class MapArrayOfEntriesImpl<K, V> implements Map<K, V> {
//
//    private static final int MAP_SIZE = 10;
//    private Entry<K, V>[] entries;
//    private int count;
//
//    public MapArrayOfEntriesImpl() {
//        this.entries = new Entry[MAP_SIZE];
//    }
//
//    @Override
//    public void put(K key, V value) {
//        for (int i = 0; i < count; i++) {
//            if (entries[i].key.equals(key)) {
//                entries[i].value = value;
//                return;
//            }
//        }
//        this.entries[count] = new Entry<>(key, value);
//        count++;
//    }
//
//    @Override
//    public V get(K key) {
//        for (int i = 0; i < count; i++) {
//            if (entries[i].key.equals(key)) {
//                return entries[i].value;
//            }
//        }
//
//        return null;
//    }
//
//    private static class Entry<K, V> {
//        K key;
//        V value;
//
//        public Entry(K key, V value) {
//            this.key = key;
//            this.value = value;
//        }
//    }
//}
