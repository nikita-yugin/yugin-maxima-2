package example5;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        try {
            URL url = new URL("https://www.w3.org/TR/PNG/iso_8859-1.txt");
            InputStream in = url.openStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);
            char[] text = new char[1024];
            inputStreamReader.read(text);
            System.out.println(Arrays.toString(text));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
