package example2.output;

import java.io.FileOutputStream;

public class Main {
    public static void main(String[] args) throws Exception {
        FileOutputStream outputStream = new FileOutputStream("output.txt");
        int codeH = 'H';
        int codeE = 'E';
        int codeL = 'L';
        int codeO = 'O';

        outputStream.write(codeH);
        outputStream.write(codeE);
        outputStream.write(codeL);
        outputStream.write(codeL);
        outputStream.write(codeO);
    }
}
