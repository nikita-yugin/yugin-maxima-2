package example2.output;

import java.io.FileOutputStream;

public class Main2 {
    public static void main(String[] args) throws Exception {
        FileOutputStream outputStream = new FileOutputStream("output.txt");
        String message = "Привет!";
        byte[] bytes = message.getBytes();
        outputStream.write(bytes);
    }
}
