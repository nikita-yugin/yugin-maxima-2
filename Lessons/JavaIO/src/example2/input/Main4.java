package example2.input;

import java.io.FileInputStream;
import java.util.Arrays;

public class Main4 {
    public static void main(String[] args) throws Exception {
        FileInputStream inputStream = new FileInputStream("input.txt");
        byte[] bytes = new byte[30];
        int count = inputStream.read(bytes, 3, 5);
        System.out.println(count);
        System.out.println(Arrays.toString(bytes));
    }
}
