package example2.input;

import java.io.FileInputStream;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws Exception {
        FileInputStream inputStream = new FileInputStream("input.txt");

        int[] bytes = new int[30];
        int current = inputStream.read();

        int position = 0;
        while (current != -1) {
            bytes[position] = current;
            position++;
            current = inputStream.read();
        }

        System.out.println(Arrays.toString(bytes));
    }
}
