package example1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("input.txt");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char[] characters;

        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;

        try {
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                char character = (char) currentByte;
                characters[position] = character;
                position++;
                currentByte = inputStream.read();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        String text = new String(characters);
        System.out.println(text);
    }
}
