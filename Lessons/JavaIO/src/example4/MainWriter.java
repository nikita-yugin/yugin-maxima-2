package example4;

import java.io.FileWriter;
import java.io.IOException;

public class MainWriter {
    public static void main(String[] args) {
        try {
            FileWriter writer = new FileWriter("output.txt");
            writer.write("Привет!");
            writer.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
