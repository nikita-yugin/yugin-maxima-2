package example4;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class MainReader {
    public static void main(String[] args) {
        try {
            FileReader reader = new FileReader("input.txt");
            char[] text = new char[10];
            reader.read(text);
            System.out.println(Arrays.toString(text));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
