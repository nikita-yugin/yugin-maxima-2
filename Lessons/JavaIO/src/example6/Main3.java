package example6;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Main3 {
    public static void main(String[] args) {
        try {
            FileWriter writer = new FileWriter("output.txt", true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write("Привет! Как дела?");
            bufferedWriter.write("Что нового?");
            bufferedWriter.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
