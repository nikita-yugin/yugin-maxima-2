package example6;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main2 {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
            System.out.println(reader.readLine());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
