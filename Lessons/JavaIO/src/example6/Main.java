package example6;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        try {
            InputStream inputStream = new FileInputStream("input.txt");
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

            int n1 = bufferedInputStream.read();
            int n2 = bufferedInputStream.read();
            System.out.println(n1 + " " + n2);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
