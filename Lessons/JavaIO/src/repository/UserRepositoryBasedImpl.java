package repository;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class UserRepositoryBasedImpl {
    private String fileName;

    public UserRepositoryBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    public List<User> findAll() {
        return null;
    }

    public Optional<User> findByFirstName(String firstName) {
        return null;
    }

    public void save(User user) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write(user.getFirstName() + "|" + user.getLastName());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                writer.close();
            } catch (IOException ignore) {}
        }
    }
}
