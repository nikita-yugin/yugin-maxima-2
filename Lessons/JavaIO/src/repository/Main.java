package repository;

public class Main {
    public static void main(String[] args) {
        UserRepositoryBasedImpl usersRepository = new UserRepositoryBasedImpl("users.txt");
        User nikita = new User("Nikita", "Yugin");
        User diana = new User("Diana", "Tursunova");
        User igor = new User("Igor", "Yugin");

        usersRepository.save(nikita);
        usersRepository.save(diana);
        usersRepository.save(igor);
    }
}
