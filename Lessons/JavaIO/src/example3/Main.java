package example3;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Main {
    public static void main(String[] args) {
        try {
            URL url = new URL("https://i.pinimg.com/originals/82/fa/63/82fa63fdba1843040c6eaceaa62fbcb3.jpg");
            InputStream inputStream = url.openStream();
            byte[] bytes = new byte[inputStream.available()];
            int count = inputStream.read(bytes);

            FileOutputStream outputStream = new FileOutputStream("image.jpg");
            outputStream.write(bytes);
            outputStream.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
