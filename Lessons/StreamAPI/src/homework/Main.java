package homework;

import homework.models.Car;
import homework.repositories.CarRepository;
import homework.repositories.CarRepositoryImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        CarRepository repository = new CarRepositoryImpl("input.txt");
        System.out.println(repository.findAll());

        List<Car> byColorOrMileage = repository.findByColorOrMileage("Black", 0);

        byColorOrMileage
                .stream()
                .map(Car::getNumber)
                .forEach(System.out::println);
    }
}
