package methods;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil(5.0);

        Stream<Integer> integerStream = Stream.of(4, 5, 10, 16, 127, 20, 46, 71);

        Predicate<Integer> predicate = NumbersUtil::isPrime;
        Function<Integer, Double> function = numbersUtil::convert;

        integerStream
                .filter(NumbersUtil::isPrime)
                .map(numbersUtil::convert)
                .forEach(System.out::println);
    }
}
