package methods;

public class NumbersUtil {

    private double number;

    public NumbersUtil(double number) {
        this.number = number;
    }

    public static boolean isPrime(int number) {
        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    public double convert(int number) {
        return number * this.number;
    }
}
