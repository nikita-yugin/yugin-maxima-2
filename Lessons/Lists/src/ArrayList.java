public class ArrayList {
    private final static int DEFAULT_SIZE = 10;
    private int[] elements;
    private int size;

    public ArrayList() {
        this.elements = new int[DEFAULT_SIZE];
    }

    /**
     * Добавление элемента в конец списка
     * @param element жлемент который мы добавляем
     */
    public void add(int element) {
        // если массив переполнен
        ensureSize();
        // добавляем элемент
        this.elements[size] = element;
    }

    /**
     * метод расширяющий массив
     */
    public void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }

    private void resize() {
        int[] newArray = new int[this.elements.length + elements.length / 2];
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * метод для получения значения лежащий под определенным индексом
     * @param index индекс под которым лежит значение
     * @return значение
     */
    public int get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.err.println("Такого индекса нет");
            return -1;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Метод очищающий массив
     */
    public void clear() {
        size = 0;
    }

    /**
     * Метод для получения размера массива
     * @return размер массива
     */
    public int size() {
        return size;
    }
}
