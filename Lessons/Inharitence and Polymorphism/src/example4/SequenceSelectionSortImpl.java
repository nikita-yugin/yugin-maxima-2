package example4;

public class SequenceSelectionSortImpl extends Sequence {
    public SequenceSelectionSortImpl(int[] array) {
        super(array);
    }

    @Override
    public void sort() {
        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;

            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
    }
}
