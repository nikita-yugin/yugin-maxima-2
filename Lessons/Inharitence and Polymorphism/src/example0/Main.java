package example0;

public class Main {

    public static void main(String[] args) {
        Database database = new Database();
        User user1 = new User(1, "user1@mail.ru", "qwerty001");
        User user2 = new User(2, "user2@mail.ru", "qwerty002");
        User user3 = new User(3, "user3@mail.ru", "qwerty003");
        User user4 = new User(4, "user4@mail.ru", "qwerty004");

        Admin admin = new Admin(0, "admin", "asdfx", new String[]{"read", "delete" , "update", "create"});

        database.addUser(user1);
        database.addUser(user2);
        database.addUser(user3);
        database.addUser(user4);

        database.authenticateUser(user1);
        database.authenticateUser(user2);
        database.authenticateUser(user3);
        database.authenticateUser(user4);

        database.authenticateAdmin(admin);
    }
}
