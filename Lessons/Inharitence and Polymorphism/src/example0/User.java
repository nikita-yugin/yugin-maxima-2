package example0;

public class User {
    private int id;
    protected String email;
    protected String password;

    public User(int id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public String[] authenticate() {
        return new String[]{email, password};
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
