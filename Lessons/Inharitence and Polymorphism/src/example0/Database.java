package example0;

public class Database {
    private User[] users;
    private int count;

    public Database() {
        this.users = new User[10];
    }

    public void addUser(User user) {
        this.users[count] = user;
        count++;
    }

    public void authenticateUser(User user) {
        String[] credentials = user.authenticate();
        for (int i = 0; i < count; i++) {
            if (credentials[0].equals(users[i].getEmail())
                    && credentials[1].equals(users[i].getPassword())) {
                System.out.println("Аутентификация пройдена - " + user.getEmail());
                return;
            }
        }

        System.err.println("Пользователь не обнаружен - " + user.getEmail());
    }

    public void authenticateAdmin(Admin admin) {
        if (admin.getEmail().equals("admin") && admin.getPassword().equals("asdfx")) {
            System.out.println("Аутентификация пройдена -  + Admin");
        } else {
            System.err.println("Аутентификация не пройдена - Admin");
        }
    }
}
