package example5;

public interface SpaceObject {
    void move(double newX, double newY, double newZ);
}
