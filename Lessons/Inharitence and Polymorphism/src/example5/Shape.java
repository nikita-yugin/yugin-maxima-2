package example5;

public interface Shape {
    void scale(double value);
}
