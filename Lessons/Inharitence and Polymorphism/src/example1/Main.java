package example1;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(0, 0, 5, 10);
        Square square = new Square(0, 0, 10);
        Ellipse ellipse = new Ellipse(5, 10, 0, 0);
        Circle circle = new Circle(10, 0, 0);

        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(circle.getPerimeter());

        rectangle.move(66, 77);
        square.move(66, 77);
        ellipse.move(66, 77);
        circle.move(66, 77);
    }
}
