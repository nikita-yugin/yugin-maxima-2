package example1;

public class Circle extends Ellipse {
    public Circle(int a, int x, int y) {
        super(a, a, x, y);
    }

    public double getPerimeter() {
        return pi * a;
    }
}
