package example1;

public class Ellipse extends Figure {
    protected int a;
    protected int b;
    protected final double pi = 3.14;

    public Ellipse(int a, int b, int x, int y) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return 2 * pi * Math.sqrt((a * a) + (b * b) / 2);
    }
}
