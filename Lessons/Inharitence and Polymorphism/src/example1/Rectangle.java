package example1;

public class Rectangle extends Figure {
    protected int a;
    protected int b;

    public Rectangle(int x, int y, int a, int b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return (this.a + this.b) * 2;
    }
}
