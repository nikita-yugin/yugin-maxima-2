package example1;

public class Square extends Rectangle {
    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }

    public double getPerimeter() {
        return this.a * 4;
    }
}
