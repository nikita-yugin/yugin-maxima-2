package example2;

public class Main {
    public static void main(String[] args) {
        Human human = new Human("Ващиль", "Петрович");
        Programmer programmer = new Programmer("Никита", "Югин");
        Student student = new Student("Диана", "Турсунова");
        Sportsman sportsman = new Sportsman("Ихарь", "Ихарь");

        human.work();
        programmer.work();
        student.work();
        sportsman.work();
    }
}
