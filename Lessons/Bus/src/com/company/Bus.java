package com.company;

public class Bus {
    private String model;
    private int number;
    private Driver driver;
    private Passenger[] passengers;
    private int count;

    public Bus(String model, int number, int placeCount) {
        this.model = model;
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.passengers = new Passenger[placeCount];
    }

    public void incomePassenger(Passenger passenger) {
        if (this.count < this.passengers.length) {
            this.passengers[count] = passenger;
            this.count++;
        } else {
            System.err.println("Автобус переполнен!");
        }
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
        driver.setBus(this);
    }

    public boolean isFull() {
        return this.count == this.passengers.length;
    }
}
