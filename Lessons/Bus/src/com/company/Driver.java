package com.company;

public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА У " + name + " НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }
}
