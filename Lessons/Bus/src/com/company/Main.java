package com.company;

public class Main {

    public static void main(String[] args) {
	    Bus bus = new Bus("Нефаз", 10, 5);
	    Driver driver = new Driver("Алексей", 5);
	    bus.setDriver(driver);
	    Passenger passenger1 = new Passenger("Максим");
	    Passenger passenger2 = new Passenger("Николай");
	    Passenger passenger4 = new Passenger("Диана");
	    Passenger passenger5 = new Passenger("Никита");

	    passenger1.goToBus(bus);
	    passenger2.goToBus(bus);
	    passenger4.goToBus(bus);
	    passenger5.goToBus(bus);

//	    driver.drive();

    }
}
