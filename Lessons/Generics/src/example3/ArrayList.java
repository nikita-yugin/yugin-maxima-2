package example3;

public class ArrayList<E> implements List<E> {
    private final static int DEFAULT_SIZE = 10;

    private E[] elements;

    private int size;

    public ArrayList() {
        this.elements = (E[]) new Object[DEFAULT_SIZE];
    }

    @Override
    public void add(E element) {
        ensureSize();

        this.elements[size++] = element;
    }

    @Override
    public E get(int index) {
        if (isCorrectIndex(index)) {
            return this.elements[index];
        } else {
            System.err.println("Элемента под данным индексом не существует");
            return null;
        }
    }

    @Override
    public int size() {
        return size;
    }

    public void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }

    public void resize() {
        E[] newArray = (E[]) new Object[this.elements.length + elements.length / 2];

        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }

        this.elements = newArray;
    }

    public boolean isFullArray() {
        return size == elements.length;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public void clear() {
        size = 0;
    }

    /**
     * Удаляет элемент в заданном индексе, смещая элементы, которые идут после удаляемого на одну позицию влево
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        for (int i = index; i < size; i++) {
            elements[i] = elements[i + 1];
        }
        size--;
    }

    /**
     * Удаляет первое вхождение элемента в список
     * @param element удаляемый элемент списка
     */
    public void remove(E element) {
        for (int i = 0; i < size; i++) {
            if (element == elements[i]) {
                removeAt(i);
                return;
            }
        }
    }

    /**
     * Удаляет последнее вхождение элемента в список
     * @param element удаляемый элемент списка
     */
    public void removeLast(E element) {
        for (int i = size; i >= 0; i--) {
            if (element == elements[i]) {
                removeAt(i);
                return;
            }
        }
    }

    /**
     * Удаляет все вхожденя данного элемента в список
     * @param element удаляемый элемент списка
     */
    public void removeAll(E element) {
        for (int i = 0; i < size - 1; i++) {
            if (element == elements[i]) {
                for (int j = i; j < size; j++) {
                    elements[j] = elements[j + 1];
                }
                size--;
            }
        }
    }
}
