package example1;

public class CoverForNokia {
    private Nokia nokia;

    public void put(Nokia nokia) {
        this.nokia = nokia;
    }

    public Nokia get() {
        return nokia;
    }
}
