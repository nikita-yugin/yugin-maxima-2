package example1;

public class Cover<T> {
    private T phone;

    public void put(T phone) {
        this.phone = phone;
    }

    public T get() {
        return this.phone;
    }
}
