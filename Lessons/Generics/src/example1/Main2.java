package example1;

public class Main2 {
    public static void main(String[] args) {
        CoverForObject coverForNokia = new CoverForObject();
        CoverForObject coverForIPhone = new CoverForObject();

        IPhone iPhone = new IPhone();
        Nokia nokia = new Nokia();


        coverForIPhone.put(iPhone);
        coverForNokia.put(nokia);

        Nokia nokiaFromCover = (Nokia) coverForNokia.get();
        IPhone iPhoneFromCover = (IPhone) coverForIPhone.get();

        nokiaFromCover.call();
        iPhoneFromCover.createPhoto();
    }
}
