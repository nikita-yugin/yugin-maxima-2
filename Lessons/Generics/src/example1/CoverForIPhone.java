package example1;

public class CoverForIPhone {
    private IPhone iPhone;

    public void put(IPhone iPhone) {
        this.iPhone = iPhone;
    }

    public IPhone get() {
        return iPhone;
    }
}
