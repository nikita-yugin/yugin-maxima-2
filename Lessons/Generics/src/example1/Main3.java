package example1;

public class Main3 {

    public static void callFromCover(CoverForObject cover) {
        Nokia nokia = (Nokia) cover.get();
        nokia.call();
    }

    public static void main(String[] args) {
        CoverForObject coverForNokia = new CoverForObject();
        CoverForObject coverForIPhone = new CoverForObject();

        Nokia nokia = new Nokia();
        IPhone iPhone = new IPhone();

        coverForIPhone.put(iPhone);
        coverForNokia.put(nokia);

//        callFromCover(CoverForNokia);

        Nokia nokiaFromCover = (Nokia) coverForNokia.get();
        IPhone iPhoneFromCover = (IPhone) coverForIPhone.get();

        nokiaFromCover.call();
        iPhoneFromCover.createPhoto();
    }
}
