package example1;

public class Main {
    public static void main(String[] args) {
        CoverForIPhone coverForIPhone = new CoverForIPhone();
        CoverForNokia coverForNokia = new CoverForNokia();

        IPhone iPhone = new IPhone();
        Nokia nokia = new Nokia();

        coverForIPhone.put(iPhone);
        coverForNokia.put(nokia);

        Nokia nokiaFromCover = coverForNokia.get();
        IPhone iPhoneFromCover = coverForIPhone.get();

        nokiaFromCover.call();
        iPhoneFromCover.createPhoto();
    }
}
