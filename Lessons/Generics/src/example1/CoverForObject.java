package example1;

public class CoverForObject {
    private Object phone;

    public void put(Object phone) {
        this.phone = phone;
    }

    public Object get() {
        return phone;
    }
}
