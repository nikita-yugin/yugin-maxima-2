package example1;

public class Main4 {

    public static void callFromCover(Cover<Nokia> cover) {
        Nokia nokia = cover.get();
        nokia.call();
    }

    public static void main(String[] args) {
        Cover<Nokia> coverForNokia = new Cover<>();
        Cover<IPhone> coverForIPhone = new Cover<>();

        Nokia nokia = new Nokia();
        IPhone iPhone = new IPhone();

        coverForIPhone.put(iPhone);
        coverForNokia.put(nokia);

        callFromCover(coverForNokia);

        Nokia nokiaFromCover = coverForNokia.get();
        IPhone iPhoneFromCover = coverForIPhone.get();

        nokiaFromCover.call();
        iPhoneFromCover.createPhoto();
    }
}
