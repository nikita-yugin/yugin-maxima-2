package example2;

public class Nullable<T> {
    private T value;

    private Nullable(T value) {
        this.value = value;
    }

    public boolean isPresent() {
        return value != null;
    }

    public T get() {
        return value;
    }

    public static <T> Nullable<T> of(T value) {
        return new Nullable<>(value);
    }

    public static <T> Nullable<T> empty() {
        return new Nullable<>(null);
    }
}
