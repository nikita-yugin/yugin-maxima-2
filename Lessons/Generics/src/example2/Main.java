package example2;

public class Main {
    public static void main(String[] args) {
        Dictionary dictionary = new Dictionary();
        dictionary.add("Hello", "Привет");
        dictionary.add("Bye", "Пока");

        Nullable<String> s1 = dictionary.translate("Hello");
        Nullable<String> s2 = dictionary.translate("Bye");
        Nullable<String> s3 = dictionary.translate("Nikita");

        if (s3.isPresent()) {
            System.out.println(s3.get().length());
        } else {
            System.err.println("Слово не найдено");
        }
    }
}
