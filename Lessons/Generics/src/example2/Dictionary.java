package example2;

public class Dictionary {
    private static final int MAX_WORDS_COUNT = 10;

    private String[] words;
    private String[] translatedWords;

    private int count;

    public Dictionary() {
        this.words = new String[MAX_WORDS_COUNT];
        this.translatedWords = new String[MAX_WORDS_COUNT];
        this.count = 0;
    }

    public void add(String word, String translate) {
        this.words[count] = word;
        this.translatedWords[count] = translate;
        count++;
    }

    public Nullable<String> translate(String word) {
        for (int i = 0; i < count; i++) {
            if (words[i].equals(word)) {
                return Nullable.of(translatedWords[i]);
            }
        }

        return Nullable.empty();
    }
}
