package example4;

import example4.animals.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main2_InheritanceInGenerics {

    public static void processListWithoutParameter(List list) {
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
        list.add(new Scanner(System.in));
        list.add(new Beast());
    }

    public static void processListWithObjectParameter(List<Object> list) {
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
        list.add(new Scanner(System.in));
        list.add(new Beast());
    }

    public static void processListWithAnimal(List<Animal> animals) {
        animals.add(new Cat());
    }

    public static void processListWithBeast(List<Beast> list) {
        for (int i = 0; i < list.size(); i++) {
            Beast object = list.get(i);
            System.out.println(object);
        }
//        list.add(new Scanner(System.in));
        list.add(new Beast());
        list.add(new Lion());
//        list.add(new Animal());
    }

    public static void main(String[] args) {
        Animal animal = new Animal();
        Beast beast = new Beast();
        Cat cat = new Cat();
        Dog dog = new Dog();
        Lion lion = new Lion();
        Tiger tiger = new Tiger();
        Wolf wolf = new Wolf();

        List<Animal> animals = new ArrayList<>();
        animals.add(beast);
        animals.add(cat);
        animals.add(dog);
        animals.add(lion);
        animals.add(tiger);
        animals.add(wolf);

        List<Beast> beasts = new ArrayList<>();
        beasts.add(beast);
        beasts.add(cat);
        beasts.add(dog);

        List<Cat> cats = new ArrayList<>();
        cats.add(cat);
        cats.add(lion);
        cats.add(tiger);

        List<Scanner> scanners = new ArrayList<>();
        scanners.add(new Scanner(System.in));
        scanners.add(new Scanner(System.in));
        scanners.add(new Scanner(System.in));

        processListWithoutParameter(animals);
        processListWithoutParameter(cats);
        processListWithoutParameter(scanners);

        List<Object> objects = new ArrayList<>();
        objects.add(animal);
        objects.add(cat);
        objects.add(new Scanner(System.in));

//        processListWithObjectParameter(animals);
//        processListWithObjectParameter(scanners);
//        processListWithObjectParameter(cats);
        processListWithObjectParameter(objects);

        processListWithBeast(beasts);
//        processListWithBeast(cats);
    }
}
