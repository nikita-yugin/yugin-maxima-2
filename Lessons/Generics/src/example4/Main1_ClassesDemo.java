package example4;

import example4.animals.*;

import java.util.ArrayList;
import java.util.List;

public class Main1_ClassesDemo {
    public static void main(String[] args) {
        Animal animal = new Animal();
        Beast beast = new Beast();
        Cat cat = new Cat();
        Dog dog = new Dog();
        Lion lion = new Lion();
        Tiger tiger = new Tiger();
        Wolf wolf = new Wolf();

        List<Animal> animals = new ArrayList<>();
        animals.add(beast);
        animals.add(cat);
        animals.add(dog);
        animals.add(lion);
        animals.add(tiger);
        animals.add(wolf);

        Animal animal1 = animals.get(2);

        List<Cat> cats = new ArrayList<>();
        cats.add(cat);
        cats.add(lion);
        cats.add(tiger);

        Cat cat1 = cats.get(1);
    }
}
