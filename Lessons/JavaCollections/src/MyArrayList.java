import java.util.Iterator;
import java.util.function.ObjDoubleConsumer;

public class MyArrayList<E> implements MyList<E> {

    private final static int DEFAULT_SIZE = 10;
    private E[] elements;
    private int size;

    public MyArrayList() {
        this.elements = (E[]) new Object[DEFAULT_SIZE];
    }

    @Override
    public void add(E element) {
        ensureSize();

        this.elements[size++] = element;
    }

    @Override
    public E get(int index) {
        return this.elements[index];
    }

    @Override
    public int size() {
        return size;
    }

    private void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }

    private void resize() {
        E[] newArray = (E[]) new Object[this.elements.length + this.elements.length / 2];

        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = this.elements[i];
        }

        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator<E> {

        private int currentIndex = 0;

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public E next() {
            E element = elements[currentIndex];
            currentIndex++;
            return element;
        }
    }
}
