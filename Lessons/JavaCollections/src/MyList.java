public interface MyList<T> extends Iterable<T> {
    void add(T element);

    T get(int index);

    int size();
}
