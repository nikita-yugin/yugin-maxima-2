public class Main {
    public static void main(String[] args) {
        MyArrayList<String> strings = new MyArrayList<>();

        strings.add("Где");
        strings.add("Кто");
        strings.add("Как");

        for (String string : strings) {
            System.out.println(string);
        }
    }
}
