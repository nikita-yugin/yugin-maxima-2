package example0;

public class Main {

    public static void main(String[] args) {
        Table table = new Table();

        table.put("Никита", 18);
        table.put("Диана", 18);
        table.put("Марсель", 27);
        table.put("Игорь", 22);
        table.put("Максим", 17);

        TablePrinter printer = new TablePrinter(table);

        while (printer.next()) {
            printer.print();
        }
    }
}
