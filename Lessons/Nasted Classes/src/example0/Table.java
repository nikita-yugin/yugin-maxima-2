package example0;

public class Table {
    private static final int MAX_COUNT_SIZE = 20;
    private TableEntry[] table;
    private int count;

    public Table() {
        this.table = new TableEntry[MAX_COUNT_SIZE];
    }

    public void put(String key, int value) {
        for (int i = 0; i < count; i++) {
            if (table[i].getKey().equals(key)) {
                table[i].setValue(value);
                return;
            }
        }

        table[count] = new TableEntry(key, value);
        count++;
    }

    public TableEntry[] getTable() {
        return table;
    }

    public int getCount() {
        return count;
    }
}
