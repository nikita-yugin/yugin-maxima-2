package example0;

public class TablePrinter {
    private Table table;
    private int currentPosition;

    public TablePrinter(Table table) {
        this.table = table;
        this.currentPosition = -1;
    }

    public boolean next() {
        if (currentPosition < table.getCount() - 1) {
            currentPosition++;
            return true;
        }
        return false;
    }

    public void print() {
        TableEntry current = table.getTable()[currentPosition];
        String currentKey = current.getKey();
        int currentValue = current.getValue();

        System.out.println("Ключ - " + currentKey + ", Значение - " + currentValue);
    }
}
