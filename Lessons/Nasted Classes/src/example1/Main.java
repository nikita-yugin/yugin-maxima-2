package example1;

public class Main {
    public static void main(String[] args) {
        Table table = new Table();

        table.put("Никита", 18);
        table.put("Диана", 18);
        table.put("Марсель", 27);
        table.put("Игорь", 22);
        table.put("Максим", 17);

        Table.Printer printer = table.new Printer();

        while (printer.next()) {
            printer.print();
        }
    }
}
