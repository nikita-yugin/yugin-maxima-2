package example1;

import java.util.Map.Entry;

public class Table {

    public static class Entry {
        private String key;
        private int value;

        public Entry(String key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    private static final int MAX_COUNT_SIZE = 10;
    private Entry[] table;
    private int count;

    public Table() {
        this.table = new Entry[MAX_COUNT_SIZE];
    }

    public void put(String key, int value) {
        for (int i = 0; i < count; i++) {
            if (table[i].key.equals(key)) {
                table[i].value = value;
                return;
            }
        }

        table[count] = new Entry(key, value);
        count++;
    }

    public class Printer {
        private int currentPosition;

        public Printer() {
            this.currentPosition = -1;
        }

        public boolean next() {
            if (currentPosition < count - 1) {
                currentPosition++;
                return true;
            }
            return false;
        }

        public void print() {
            Entry current = table[currentPosition];
            String currentKey = current.key;
            int currentValue = current.value;
            System.out.println("Ключ - " + currentKey + ", Значение - " + currentValue);
        }
    }
}
