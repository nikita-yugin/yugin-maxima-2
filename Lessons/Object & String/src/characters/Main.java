package characters;

public class Main {
    public static void main(String[] args) {
        char a = 'A';
        char b = 'B';
        char ы = 'Ы';

        int codeA = a;
        int codeB = b;
        int codeЫ = ы;
        char c = (char) 67;

        System.out.println(codeA);
        System.out.println(codeB);
        System.out.println(codeЫ);
        System.out.println(c);
    }
}
