package characters;

import java.util.Scanner;

public class Main4 {
    public static void toLowerCase(char[] text) {
        for (int i = 0; i < text.length; i++) {
            if (isUpperLetter(text[i])) {
                text[i] = (char)(text[i] + 32);
            }
        }
    }

    public static int parseInt(char[] numbers) {
        int mult = 1;
        int result = 0;
        for (int i = numbers.length - 1; i >= 0; i--) {
            if (isDigit(numbers[i])) {
                result += (numbers[i] - '0') * mult;
                mult *= 10;
            }
        }

        return result;
    }

    public static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private static boolean isUpperLetter(char c) {
        return c >= 'A' && c <= 'Z';
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] text = scanner.nextLine().toCharArray();
        int result = parseInt(text);
        System.out.println(result + 1);
    }
}
