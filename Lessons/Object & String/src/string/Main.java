package string;

public class Main {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = s1.replaceAll("l", "L");
        System.out.println(s2);
        System.out.println(s1 == s2);
    }
}
