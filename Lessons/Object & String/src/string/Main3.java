package string;

import java.util.Random;

public class Main3 {
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();

        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            int number = random.nextInt(10000);
            String numberAsString = String.valueOf(number);
            stringBuilder.append(numberAsString);
        }

        String text = stringBuilder.toString();
        System.out.println(text);
    }
}
