package object;

import java.util.Scanner;

public class Main {

    public static void objectProcess(Object object) {
        System.out.println(object.getClass());
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human human = new Human("Никита", 18, 1.85);
        Batmen batmen = new Batmen("Брюс", 45, 1.84);

        objectProcess(scanner);
        objectProcess(human);
        objectProcess(batmen);
    }
}
