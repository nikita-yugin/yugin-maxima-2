package object;

import java.util.Scanner;

public class Main2 {

    public static boolean allEquals(Object[] objects) {
        for (int i = 1; i < objects.length; i++) {
            if (!objects[i - 1].equals(objects[i])) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        Human human1 = new Human("Никита", 18, 1.85);
        Human human2 = new Human("Никита", 18, 1.85);
        Human human3 = new Human("Никита", 18, 1.85);
        Scanner scanner = new Scanner(System.in);

        Object[] objects = {human1, human2, human3};
        Object[] objects1 = {"Hello", "Hello", "Hello"};

        System.out.println(allEquals(objects));
        System.out.println(allEquals(objects1));

        System.out.println(human1 == human2); // false
        System.out.println(human1.equals(human2)); // true
        System.out.println(human1.equals(scanner)); // false
        System.out.println(human1.equals(human1)); // true
    }
}
