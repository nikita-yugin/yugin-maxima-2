package final_keyword;

import object.Human;

public class MainFinalReferenceVariables {
    public static void main(String[] args) {
        final int[] array = {10, 77, 100, 85};
        final Human human = new Human("Никита", 18, 1.85);

//        array = new int[10]; не могу заменит ссылку
//        human = new Human("Виктор", 18, 1.85);

        human.setAge(19);
        human.setHeight(1.86);
        array[2] = 55;
    }
}
