package final_keyword;

public class MainFinalPrimitiveVariables {
    public static void main(String[] args) {
        final int x = 10;
        System.out.println(x);
//        x = 15;
    }
}
