drop table if exists account;
drop table if exists car;
drop table if exists driver_car;

create table account
(
    id         serial primary key,
    first_name char(20),
    last_name  char(20),
    experience int default 0 not null check ( experience > -1 and experience < 99 )
);

create table car
(
    id       serial primary key,
    model    varchar(20),
    color    varchar(20),
    owner_id bigint
);

create table driver_car
(
    car_id      bigint,
    driver_id   bigint,
    start_date  timestamp,
    finish_date timestamp
);

alter table driver_car
    add foreign key (car_id) references car (id);

alter table driver_car
    add foreign key (driver_id) references account (id);

alter table car
    add foreign key (owner_id) references account (id);

alter table account add email varchar(255) unique;
alter table account add password varchar(255);
