-- получить всех водителей
select *
from account;

--получить всех водителей в отсортированном виде по возрастанию id
select *
from account
order by id;

-- получить всех водителей по убыванию их опыта вождения, если опыта нет у нескольких - по наличию прав
select *
from account
order by experience desc, has_licence desc;

-- получить имена тех, у кого нет прав
select first_name
from account
where has_licence = false;

-- получить опыт и имена тех, у кого опыт больше 10-ти, но меньше 20-ти лет
select first_name, experience
from account
where experience between 10 and 20;

-- получить количество людей без прав
select count(*)
from account
where has_licence = false;

-- получить кол-во людей с лицензией и без
select has_licence, count(*)
from account
group by has_licence;

--получить кол-во людей с определенным стажем
select experience, count(*)
from account
group by experience;

-- получить все варианты стажа их таблицы
select distinct experience
from account;

-- получить средний стаж водителей имеющих лицензию
select avg(experience)
from account
where has_licence = true;

-- получить всех владельцев, у которых есть хотя бы одна машина
select id, first_name
from account
where id in (select distinct owner_id from car where owner_id notnull)
order by id;

-- получить всех владельцев, у которых есть хотя бы одна машина красного цвета
select id, first_name
from account
where id in (select owner_id from car where color = 'Red');

-- получить имена владельцев, у которых более одной машины
select id, first_name
from account
where id in (select owner_id
             from (select owner_id, count(id) as cars_count from car where owner_id notnull group by owner_id) as counts
             where counts.cars_count > 1);

-- возвращает владельцев и кол-во их машин
select owner_id, count(id) as cars_count
from car
where owner_id notnull
group by owner_id;

-- получить имя владельца машины, которую арендуют больше 1 раза
select first_name
from account
where id in (select owner_id
             from car
             where id in (select car_id
                          from (select car_id, count(driver_id) as driver_count
                                from driver_car
                                group by (car_id)) as cars
                          where cars.driver_count > 1));

-- получить всех владельцев и их машины (получили всех владельцев, включая тех, у кого нет машины)
select *
from account a
         left join car c on a.id = c.owner_id;

-- получить всех владельцев и их машины (получили все машины, включая тех, у которых нет владельцев)
select *
from account a
         right join car c on a.id = c.owner_id;

-- получить всех владельцев и их машины (получили только тех владельцев, у которых есть машины)
select *
from account a
         inner join car c on a.id = c.owner_id;

-- получить всех владельцев и машины (получили всех владельцев, включая тех, у кого нет машины,
-- так же получили все машины, включая те, у которых нет владельца)
select *
from account a
         full outer join car c on a.id = c.owner_id;

