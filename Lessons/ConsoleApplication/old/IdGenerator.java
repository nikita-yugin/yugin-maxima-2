package Application.util;

public interface IdGenerator {
    Long generate();
}
