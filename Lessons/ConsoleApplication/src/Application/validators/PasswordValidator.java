package Application.validators;

import Application.validators.exceptions.PasswordValidatorException;

public interface PasswordValidator {
    void validate(String password) throws PasswordValidatorException;
}
