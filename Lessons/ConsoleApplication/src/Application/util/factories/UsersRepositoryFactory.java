package Application.util.factories;

import Application.repositories.UsersRepository;
import Application.repositories.UsersRepositoryJdbcImpl;
import Application.util.jdbc.DataSourceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class UsersRepositoryFactory {

    public static UsersRepository onJdbc() {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new DataSourceImpl(properties);

        return new UsersRepositoryJdbcImpl(dataSource);
    }
}
