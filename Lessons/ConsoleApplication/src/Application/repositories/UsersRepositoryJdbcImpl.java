package Application.repositories;

import Application.models.User;
import Application.util.jdbc.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryJdbcImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_INSERT_INTO_ACCOUNT = "insert into account(first_name, last_name, email, password)" +
            "values (?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "select * from account " +
            "where email = ? limit 1";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from account " +
            "where id = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_HAS_LICENCE = "select * from account " +
            "where has_licence = ?";

    //language=SQL
    private static final String SQL_UPDATE_ACCOUNT_BY_ID = "update account set first_name = ?, " +
            "last_name = ?, email = ?, password = ? where id = ?";

    private final DataSource dataSource;

    public UsersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final RowMapper<User> usersRowMapper = (RowMapper<User>) row -> new User(
        row.getLong("id"),
        row.getString("first_name"),
        row.getString("last_name"),
        row.getString("email"),
        row.getString("password"));

    @Override
    public void save(User user) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     SQL_INSERT_INTO_ACCOUNT,
                     Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert user");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (!generatedKeys.next()) {
                throw new SQLException("Can't retrieve generated id");
            }

            Long generatedId = generatedKeys.getLong("id");

            user.setId(generatedId);

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<User> findOneByEmail(String email) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_EMAIL)) {
            statement.setString(1, email);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    User user = usersRowMapper.mapRow(resultSet);

                    if (resultSet.next()) {
                        throw new SQLException("More than one rows");
                    }

                    return Optional.of(user);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<User> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    User user = usersRowMapper.mapRow(resultSet);

                    if (resultSet.next()) {
                        throw new SQLException("More than one rows");
                    }
                    return Optional.of(user);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<User> findAllByHasLicence(boolean hasLicence) {
        List<User> users = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_BY_HAS_LICENCE)) {

            statement.setBoolean(1, hasLicence);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    User user = usersRowMapper.mapRow(resultSet);

                    users.add(user);
                }

                return users;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(User user) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ACCOUNT_BY_ID)) {

            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.setLong(5, user.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update user");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
