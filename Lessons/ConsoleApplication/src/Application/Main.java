package Application;

import Application.repositories.UsersRepository;
import Application.services.UsersService;
import Application.services.UsersServiceImpl;
import Application.util.factories.UsersRepositoryFactory;
import Application.validators.EmailFormatValidator;
import Application.validators.EmailValidator;
import Application.validators.PasswordLengthValidator;
import Application.validators.PasswordValidator;

public class Main {
    public static void main(String[] args) {

        UsersRepository repository = UsersRepositoryFactory.onJdbc();

        EmailValidator emailValidator = new EmailFormatValidator();
        PasswordValidator passwordValidator = new PasswordLengthValidator();
        UsersService usersService = new UsersServiceImpl(repository, emailValidator, passwordValidator);

//        usersService.signUp("User3", "User3", "user3@email.com", "qwerty009");
        usersService.signIn("user3@email.com", "qwerty009");
    }
}