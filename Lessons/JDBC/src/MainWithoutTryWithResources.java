import java.sql.*;

public class MainWithoutTryWithResources {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/maxima2";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty007";

    //language=SQL
    private static final String SQL_SELECT_FROM_ACCOUNT = "select * from account order by id";

    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_SELECT_FROM_ACCOUNT);

            while (resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                boolean hasLicence = resultSet.getBoolean("has_licence");

                System.out.println(firstName + " " + lastName + " " + hasLicence);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignore) {}
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignore) {}
            }

            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignore) {}
            }
        }
    }
}