import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class MainPreparedStatement {

    //language=SQL
    private static final String SQL_INSERT_INTO_ACCOUNT = "insert into account(id, first_name, last_name, experience, has_licence)" +
            " values (?, ?, ?, ?)";

    public static void main(String[] args) {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        Scanner scanner = new Scanner(System.in);

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_ACCOUNT)) {

            String firstName = scanner.nextLine();
            String lastName = scanner.nextLine();
            int experience = scanner.nextInt();
            scanner.nextLine();
            boolean hasLicence = scanner.hasNextLine();

            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setInt(3, experience);
            statement.setBoolean(4, hasLicence);

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
