import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class MainWithTryWithResources {

    //language=SQL
    private static final String SQL_SELECT_FROM_ACCOUNT = "select * from account order by id";
    public static void main(String[] args) {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
                Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_ACCOUNT)) {
                while (resultSet.next()) {
                    String firstName = resultSet.getString("first_name");
                    String lastName = resultSet.getString("last_name");
                    boolean hasLicence = resultSet.getBoolean("has_licence");

                    System.out.println(firstName + " " + lastName + " " + hasLicence);
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
