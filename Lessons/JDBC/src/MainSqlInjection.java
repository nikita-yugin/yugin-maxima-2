import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class MainSqlInjection {
    public static void main(String[] args) {

        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        Scanner scanner = new Scanner(System.in);

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             Statement statement = connection.createStatement()) {

            String firstName = scanner.nextLine();
            String lastName = scanner.nextLine();
            int experience = scanner.nextInt();
            scanner.nextLine();
            boolean hasLicence = scanner.hasNextLine();

            String sql = "insert into account(first_name, last_name, experience, has_licence) values (" +
                    "'" + firstName + "','" + lastName + "', " + experience + ", " + hasLicence + ");";

            System.out.println(sql);
            int affectedRows = statement.executeUpdate(sql);
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
