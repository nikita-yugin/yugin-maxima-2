import java.util.Arrays;

public class Main {

    public static void sort(int[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] a = {54, 12, 100, -10, 11, 56, 19, -1, 13, -9};
        sort(a);
        System.out.println(Arrays.toString(a));
    }
}
