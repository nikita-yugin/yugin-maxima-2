import java.util.Comparator;

public class HumanByWeightComparator implements Comparator<Human> {
    @Override
    public int compare(Human o1, Human o2) {
        return o1.getWeight() - o2.getWeight();
    }
}
