import java.util.Arrays;

public class Main2 {

    public static <T extends Comparable<T>> void sort(T[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        Human nikita = new Human("Nikita", 182, 67, 18);
        Human diana = new Human("Diana", 162, 45, 18);
        Human igor = new Human("Igor", 185, 75, 23);

        String[] words = {"Nikita", "Diana", "Igor"};

        Human[] humans = {nikita, diana, igor};

        System.out.println(nikita.compareTo(diana));
        System.out.println(nikita.compareTo(igor));

        System.out.println(Arrays.toString(words));
        System.out.println(Arrays.toString(humans));
    }
}
