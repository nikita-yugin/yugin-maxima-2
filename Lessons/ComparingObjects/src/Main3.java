import java.util.Arrays;
import java.util.Comparator;

public class Main3 {

    public static <T> void sort(T[] array, Comparator<T> comparator) {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (comparator.compare(array[j], array[j + 1]) > 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        Human nikita = new Human("Nikita", 182, 67, 18);
        Human diana = new Human("Diana", 162, 45, 18);
        Human igor = new Human("Igor", 185, 75, 23);

        Human[] humans = {nikita, diana, igor};

        HumanByAgeComparator humanByAgeComparator = new HumanByAgeComparator();
        System.out.println(humanByAgeComparator.compare(nikita, diana));
        System.out.println(humanByAgeComparator.compare(nikita, igor));

        HumanByWeightComparator humanByWeightComparator = new HumanByWeightComparator();
        System.out.println(humanByWeightComparator.compare(nikita, diana));
        System.out.println(humanByWeightComparator.compare(nikita, igor));

        sort(humans, humanByAgeComparator);
        System.out.println(Arrays.toString(humans));
        sort(humans, humanByWeightComparator);
        System.out.println(Arrays.toString(humans));
    }
}
