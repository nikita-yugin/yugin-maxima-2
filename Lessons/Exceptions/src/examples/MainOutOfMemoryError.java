package examples;

public class MainOutOfMemoryError {
    public static void main(String[] args) {
        int[] array = new int[Integer.MAX_VALUE];

        /*
        Exception in thread "main" java.lang.OutOfMemoryError: Requested array size exceeds VM limit
	        at examples.MainOutOfMemoryError.main(MainOutOfMemoryError.java:5)
         */
    }
}
