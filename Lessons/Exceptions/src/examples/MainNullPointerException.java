package examples;

import java.util.Arrays;

public class MainNullPointerException {

    public static void reverse(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[array.length - 1 - i];
            array[array.length - 1 - i] = array[i];
            array[i] = temp;
        }
    }

    public static void main(String[] args) {
        int[] b = null;
        int[] a = {1, 2, 3, 4, 5};
        reverse(b);
        System.out.println(Arrays.toString(a));

        /*
        Exception in thread "main" java.lang.NullPointerException: Cannot read the array length because "array" is null
            at examples.MainNullPointerException.reverse(MainNullPointerException.java:9)
            at examples.MainNullPointerException.main(MainNullPointerException.java:21)
         */
    }
}
