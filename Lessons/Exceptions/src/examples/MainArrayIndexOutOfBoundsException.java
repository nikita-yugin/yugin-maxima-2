package examples;

public class MainArrayIndexOutOfBoundsException {

    public static void main(String[] args) {
        int[] array = {0, 1, 2};
        array[3] = 5;

        /*
        Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 3 out of bounds for length 3
	        at examples.MainArrayIndexOfBoundsException.main(MainArrayIndexOfBoundsException.java:7)
         */
    }
}
