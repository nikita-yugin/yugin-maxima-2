package examples;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainNoSuchElementException {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("Hello");
        words.add("Nikita");

        Iterator<String> iterator = words.iterator();
        System.out.println(iterator.next());
        System.out.println(iterator.next());
        System.out.println(iterator.next());

        /*
        Exception in thread "main" java.util.NoSuchElementException
            at java.base/java.util.ArrayList$Itr.next(ArrayList.java:970)
            at examples.MainNoSuchElementException.main(MainNoSuchElementException.java:16)
         */
    }
}
