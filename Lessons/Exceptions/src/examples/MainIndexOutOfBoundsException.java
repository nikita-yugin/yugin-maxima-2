package examples;

import java.util.ArrayList;
import java.util.List;

public class MainIndexOutOfBoundsException {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("Hello");
        System.out.println(words.get(1));

        /*
        Exception in thread "main" java.lang.IndexOutOfBoundsException: Index 1 out of bounds for length 1
            at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
            at java.base/java.util.Objects.checkIndex(Objects.java:359)
            at java.base/java.util.ArrayList.get(ArrayList.java:427)
            at examples.MainIndexOutOfBoundsException.main(MainIndexOutOfBoundsException.java:10)
         */
    }
}
