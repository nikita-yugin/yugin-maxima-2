package examples;

import java.util.Scanner;

public class MainArithmeticException {

    public static int div(int a, int b) {
        return a / b;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int c = div(a, b);

        System.out.println(c);

        /*
        Exception in thread "main" java.lang.ArithmeticException: / by zero
	        at examples.MainArithmeticException.div(MainArithmeticException.java:8)
	        at examples.MainArithmeticException.main(MainArithmeticException.java:16)
         */
    }
}
