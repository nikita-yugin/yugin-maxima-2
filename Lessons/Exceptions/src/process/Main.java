package process;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static int div(int a, int b) {
        return a / b;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            try {
                int a = scanner.nextInt();
                int b = scanner.nextInt();
                int c = div(a, b);
                System.out.println(c);
            } catch (ArithmeticException e) {
                System.out.println("Ошибка - " + e.getMessage() + ", нельзя делить на ноль!");
            } catch (InputMismatchException e) {
                System.out.println("Вводить слова нельзя, попробуйте цифры.");
                scanner.nextLine();
            }
        }
    }
}
