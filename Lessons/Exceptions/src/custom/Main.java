package custom;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();
        if (password.length() < 7) {
            throw new PasswordNotCorrectException("Маленькая длина пароля");
        }

        /*
        Exception in thread "main" custom.PasswordNotCorrectException: Маленькая длина пароля
	        at custom.Main.main(Main.java:10)
         */
    }
}
