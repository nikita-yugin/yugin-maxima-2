package unchecked;

import java.util.Scanner;

public class UsersScanner {
    private Scanner scanner;

    public UsersScanner() {
        this.scanner = new Scanner(System.in);
    }

     public User nextUser() {
        String name = scanner.next();
        if (scanner.hasNextInt()) {
            int age = scanner.nextInt();
            scanner.nextLine();
            return new User(name, age);
        } else {
            throw new IncorrectAgeException();
        }
     }
}
