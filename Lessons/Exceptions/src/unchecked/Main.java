package unchecked;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersScanner scanner = new UsersScanner();
        List<User> users = new ArrayList<>();

        User user1 = scanner.nextUser();
        User user2 = scanner.nextUser();
        User user3 = scanner.nextUser();

        users.add(user1);
        users.add(user2);
        users.add(user3);

        System.out.println(users);
    }
}
