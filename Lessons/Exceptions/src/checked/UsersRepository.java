package checked;

import java.util.ArrayList;
import java.util.List;

public class UsersRepository {
    private UsersScanner usersScanner;

    public UsersRepository(UsersScanner usersScanner) {
        this.usersScanner = usersScanner;
    }

    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        User current = null;
        try {
            current = usersScanner.nextUser();
        } catch (IncorrectAgeException e) {
            throw new IllegalArgumentException(e);
        }

        while (current != null) {
            users.add(current);
            try {
                current = usersScanner.nextUser();
            } catch (IncorrectAgeException e) {
                throw new IllegalArgumentException(e);
            }
        }

        return users;
    }
}
