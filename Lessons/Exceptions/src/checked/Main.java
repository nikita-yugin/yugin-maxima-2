package checked;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersScanner scanner = new UsersScanner("input.txt");
        scanner.open();

        UsersRepository userRepository = new UsersRepository(scanner);

        List<User> users = userRepository.findAll();
        System.out.println(users);
    }
}
