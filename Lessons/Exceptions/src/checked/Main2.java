package checked;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class Main2 {
    public static void main(String[] args) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("input2.txt");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char[] characters = null;

        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            e.printStackTrace();
        }

        int i = 0;
        try {
            while (inputStream.available() != 0) {
                char character = (char) inputStream.read();
                characters[i] = character;
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(Arrays.toString(characters));
    }
}
