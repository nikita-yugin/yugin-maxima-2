package checked;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class UsersScanner {
    private String fileName;
    private Scanner scanner;

    public UsersScanner(String fileName) {
        this.fileName = fileName;
    }

    public void open() {
        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            this.scanner = new Scanner(fileInputStream);
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка - " + e.getMessage());
        }
    }

    public User nextUser() throws IncorrectAgeException {
        if (scanner.hasNext()) {
            String name = scanner.next();
            if (scanner.hasNextInt()) {
                int age = scanner.nextInt();
                scanner.nextLine();
                return new User(name, age);
            } else {
                throw new IncorrectAgeException();
            }
        } else {
            return null;
        }
    }
}
