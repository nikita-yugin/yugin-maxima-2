package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int size = scanner.nextInt();

        // инициализируем массив
        int[] array = new int[size];
        System.out.println("Заполните массив: ");
        // заполняем массив числами из консоли
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Получившийся массив: " + Arrays.toString(array));
        // спрашиваем у пользователя, хочет он получить сумму всего массива или промежутка
        System.out.println("Вы хотите узнать сумму всего массива или промежутка? 0 - всего массива, 1 - промежутка");
        // считываем введённое пользователем число
        int number = scanner.nextInt();
        // программа выводит сумму в определённом диапазоне
        if (number == 1) {
            System.out.println("Укажите начало диапазона суммирования: ");
            int from = scanner.nextInt();
            System.out.println("Укажите конец диапазона суммирования: ");
            int to = scanner.nextInt();
            System.out.println("Сумма элементов массива в заданном диапазоне: " + getAverage(array, from, to));
        // программа выводит сумму всего массива
        } else {
            System.out.println("Сумма элементов всего массива: " + getAverage(array));
        }
    }

    // ф-ия выполняющая суммирование всех элементов массива
    public static double getAverage(int[] array) {
        double sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }

        return sum;
    }

    // ф-ия выполняющая суммирование заданного промежутка массива
    public static double getAverage(int[] array, int from, int to) {
        double sum = 0;

        for (int i = from; i <= to; i++) {
            sum += array[i];
        }

        return sum;
    }

}
