package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        swap(array, a, b);
        System.out.println(Arrays.toString(array));

//        int c = 10;
//        int d = 5;
//        swap(c, d);
    }

    // не работает, т.к данные передаются не по ссылке, а по значению
    public static void swap(int x, int y) {
        int temp = x;
        x = y;
        y = temp;
    }

    // работвет, т.к элементы массива всегда передаются по ссылке
    public static void swap(int[] array, int x, int y) {
        int temp = array[x];
        array[x] = array[y];
        array[y] = temp;
    }
}
