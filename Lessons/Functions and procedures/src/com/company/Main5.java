package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое число: ");
        int a = scanner.nextInt();
        System.out.println("Введите второе число: ");
        int b = scanner.nextInt();
        printInRange(a, b);

        System.out.println("Введиде размер массива: ");
        int size = scanner.nextInt();
        int[] array = new int[size];

        System.out.println("Заполните массив: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));

        System.out.println("Введите первый элемент: ");
        int from = scanner.nextInt();
        System.out.println("Введите второй элемент: ");
        int to = scanner.nextInt();
        printInRange(array, from, to);

    }

    // выводит элементы в заданном диапазоне
    public static void printInRange(int from, int to) {
        for (int i = from; i <= to; i++) {
            System.out.print(i + " ");
        }
    }

    // выводит элементы массива в определённом диапазоне
    public static void printInRange(int[] array, int from, int to) {
        for (int i = from; i <= to; i++) {
            System.out.print(array[i] + " ");
        }
    }

}
