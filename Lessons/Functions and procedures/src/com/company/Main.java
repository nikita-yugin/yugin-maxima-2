package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        while (number != -1) {
            int digitsSum = getDigitsSum(number);
            System.out.println(digitsSum);
            number = scanner.nextInt();
        }
    }

    // функция находящая сумму чисел
    public static int getDigitsSum(int number) {
        int digitsSum = 0;

        while (number != 0) {
            int lastDigit = number % 10;
            number /= 10;
            digitsSum += lastDigit;
        }

        return digitsSum;
    }

}
