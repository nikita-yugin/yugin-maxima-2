package com.company;

import java.util.Scanner;

public class Main2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ВВедите число начала диапазона суммирования: ");
        int from = scanner.nextInt();
        System.out.println("Введите число конца диапазона суммирования: ");
        int to = scanner.nextInt();
        System.out.println("Сумма чисел диапазона: " + getSumFromRange(from, to));
    }

    // функция которая суммирует все числа заданного диапазона
    public static int getSumFromRange(int from, int to) {
        int sum = 0;

        for (int i = from; i <= to; i++) {
            sum += i;
        }

        return sum;
    }

}
