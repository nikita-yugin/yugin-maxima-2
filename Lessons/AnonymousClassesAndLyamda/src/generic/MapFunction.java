package generic;

public interface MapFunction<T, R> {
    R map(T value);
}
