package generic;

public class DataProcessor {
    /**
     * Метод конвертирует один массив в другой
     * @param array входной массив
     * @return выходной массив
     */
    public static <T, R> R[] map(T[] array, MapFunction<T, R> function) {
        R[] newArray = (R[]) new Object[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = function.map(array[i]);
        }

        return newArray;
    }
}
