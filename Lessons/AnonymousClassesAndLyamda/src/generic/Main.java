package generic;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] lines = {"Привет", "Как дела?", "Что нового?", "Расскажи"};
        Object[] lengths = DataProcessor.map(lines, String :: length);
        System.out.println(Arrays.toString(lengths));
    }
}
