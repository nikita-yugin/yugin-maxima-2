package lambda;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {432, 67, 23, 1111, 7654, 123};

//        IntegerMapFunction digitsSum = new IntegerMapFunction() {
//            @Override
//            public int mapInt(int value) {
//                // складываем сумму цифр сюда
//                int digitsSum = 0;
//                // вычисляем сумму цифр
//                while (value != 0) {
//                    digitsSum += value % 10;
//                    value = value / 10;
//                }
//                // возвращаем сумму цифр
//                return digitsSum;
//            }
//        };

//        IntegerMapFunction mod2 = new IntegerMapFunction() {
//            @Override
//            public int mapInt(int value) {
//                return value % 2;
//            }
//        };

        int[] newArray = NumbersProcessor.map(array, value -> {
            int result = 0;

            while (value != 0) {
                result += value % 10;
                value = value / 10;
            }

            return result;
        });

        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(newArray));
    }
}
