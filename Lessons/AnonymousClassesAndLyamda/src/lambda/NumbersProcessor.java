package lambda;

public class NumbersProcessor {
    /**
     * Метод конвертирует один массив в другой
     * @param array входной массив
     * @return выходной массив
     */
    public static int[] map(int[] array, IntegerMapFunction function) {
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = function.mapInt(array[i]);
        }

        return newArray;
    }
}
