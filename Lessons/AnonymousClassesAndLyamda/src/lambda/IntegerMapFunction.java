package lambda;

public interface IntegerMapFunction {
    int mapInt(int value);
}
