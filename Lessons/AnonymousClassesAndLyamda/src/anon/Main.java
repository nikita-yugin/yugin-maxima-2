package anon;

public class Main {
    public static void main(String[] args) {
        TextField textField = new TextField() {
            private final String[] history = new String[10];
            private int count = 0;

            @Override
            public void clear() {
                if (this.text != null) {
                    history[count] = this.text;
                    count++;
                }
            }

            @Override
            public String onPressText(String text) {
                // autocomplete
                for (int i = 0; i < count; i++) {
                    if (history[i].startsWith(text)) {
                        return history[i];
                    }
                }

                return text;
            }
        };

        textField.setText("Привет");
        textField.setText("Пока");
        textField.setText("Никита");

        textField.setText("При");
        System.out.println(textField.getText());
        textField.setText("Ник");
        System.out.println(textField.getText());
    }
}
