package anon;


// абстрактный класс, описывающий простое текстовое поле
public abstract class TextField {
    // поле хранящее текст
    protected String text;

    // метод добавления текста
    public void setText(String text) {
        // прежде чем добавить новое поле, очистим
        clear();
        // прежде чем положим текст, мы его можем модифицировать
        this.text = onPressText(text);
    }

    // геттер
    public String getText() {
        return text;
    }

    // абстрактные методы, которые при желании могут реализовать другие разработчики
    // сделать свою реализацию
    public abstract void clear();

    public abstract String onPressText(String text);
}
