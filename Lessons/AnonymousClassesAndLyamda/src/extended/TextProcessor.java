package extended;

public class TextProcessor {
    public static String processText(String[] lines, TwoArgumentsFunction<String, String> function) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < lines.length - 1; i++) {
            builder.append(function.map(lines[i], lines[i + 1]));
        }
        return builder.toString();
    }
}
