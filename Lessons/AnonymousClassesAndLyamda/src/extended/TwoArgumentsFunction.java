package extended;

public interface TwoArgumentsFunction<T, R> {
    R map(T arg1, T arg2);
}
