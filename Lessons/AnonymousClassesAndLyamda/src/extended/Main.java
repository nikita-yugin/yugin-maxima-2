package extended;

public class Main {
    public static void main(String[] args) {
        String[] lines = {"Привет", "Как дела?", "Что нового?", "Расскажи!"};

        String result = TextProcessor.processText(lines, (stringA, stringB) -> stringA + "#" + stringB);
        System.out.println(result);
    }
}
