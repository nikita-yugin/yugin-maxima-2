public class Main {

    public static int g(int n, int m, int k) {
        System.out.println("->> g(" + n + ", " + m + ", " + k + ")");
        int result = f(n, m, k) - e(n, m, k);
        System.out.println("<<- g() = " + result);
        return result;
    }

    public static int f(int n, int m, int k) {
        System.out.println("->> f(" + n + ", " + m + ", " + k + ")");
        int result = b(n, m) + c(m, k);
        System.out.println("<<- f() = " + result);
        return result;
    }

    public static int e(int n, int m, int k) {
        System.out.println("->> e(" + n + ", " + m + ", " + k + ")");
        int result = a(n, m) * b(m, k);
        System.out.println("<<- e() = " + result);
        return result;
    }

    public static int c(int x, int y) {
        System.out.println("->> c(" + x + ", " + y + ")");
        int result = x * y;
        System.out.println("<<- c() = " + result);
        return result;
    }

    public static int b(int x, int y) {
        System.out.println("->> b(" + x + ", " + y + ")");
        int result = x - y;
        System.out.println("<<- b() = " + result);
        return result;
    }

    public static int a(int x, int y) {
        System.out.println("->> a(" + x + ", " + y + ")");
        int result = x + y;
        System.out.println("<<- a() = " + result);
        return result;
    }

    public static void main(String[] args) {
        System.out.println(g(10, 20, 30));
    }
}
