public class Main2 {

    public static int f(int n) {
        if (n == 0) {
            System.out.println("->> f(" + n + ")");
            int result = 1;
            System.out.println("<<- f() = " + result);
            return result;
        } else {
            System.out.println("->> f(" + n + ")");
            int result = f(n - 1) * n;
            System.out.println("<<- f() = " + n);
            return result;
        }
    }

    public static void main(String[] args) {
        System.out.println(f(5));
    }

}
