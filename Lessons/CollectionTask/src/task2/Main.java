package task2;

public class Main {
    public static void main(String[] args) {
        User nikita = new User("+79998887766", "Nikita");
        User diana = new User("+79987765544", "Diana");

        Bank bank = new Bank();

        bank.sendMoney(nikita, diana, 2000);
        bank.sendMoney(nikita, diana, 200);
        bank.sendMoney(nikita, diana, 300);
        bank.sendMoney(diana, nikita, 300);
        bank.sendMoney(diana, nikita, 200);
        bank.sendMoney(diana, nikita, 2000);

        System.out.println(bank.getTransactionsByUser(nikita));
        System.out.println(bank.getTransactionsByUser(diana));
    }
}
