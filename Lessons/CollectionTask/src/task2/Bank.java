package task2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bank {
    private Map<User, List<Transaction>> transactions;

    public Bank() {
        this.transactions = new HashMap<>();
    }

    public void sendMoney(User from, User to, int sum) {
        Transaction transaction = new Transaction(from, to, sum);

        if (!transactions.containsKey(from)) {
            transactions.put(from, new ArrayList<>());
        }

        transactions.get(from).add(transaction);
    }

    public List<Transaction> getTransactionsByUser(User user) {
        return transactions.get(user);
    }
}
