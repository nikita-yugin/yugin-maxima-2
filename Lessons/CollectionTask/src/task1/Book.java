package task1;

public class Book {
    private String title;
    private String author;
    private int createdYear;

    public Book(String title, String author, int createdYear) {
        this.title = title;
        this.author = author;
        this.createdYear = createdYear;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getCreatedYear() {
        return createdYear;
    }

    @Override
    public String toString() {
        return "Название: " + title + ", автор: " + author + ", год публикации: " + createdYear;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof Book) {
            Book that = (Book) obj;
            return this.title.equals(that.title) &&
                    this.author.equals(that.author) &&
                    this.createdYear == that.createdYear;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this.title.hashCode() + this.author.hashCode() * 31 + createdYear * 31 * 31;
    }
}
