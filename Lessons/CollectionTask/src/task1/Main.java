package task1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Map<Book, String> books = new HashMap<>();

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("1. Добавить книгу");
            System.out.println("2. Посмотреть краткое содержание");

            int command = scanner.nextInt();
            scanner.nextLine();
            if (command == 1) {
                String title = scanner.nextLine();
                String author = scanner.nextLine();
                int createdYear = scanner.nextInt();
                scanner.nextLine();
                String description = scanner.nextLine();

                Book book = new Book(title, author, createdYear);
                books.put(book, description);
            } else if (command == 2) {
                String title = scanner.nextLine();
                String author = scanner.nextLine();
                int createdYear = scanner.nextInt();
                scanner.nextLine();
                Book book = new Book(title, author, createdYear);
                if (books.containsKey(book)) {
                    System.out.println(books.get(book));
                } else {
                    System.out.println("Книга не обнаружена!");
                }
            }
        }
    }
}
