package Application;

import Application.models.User;
import Application.repositories.UsersRepository;
import Application.util.factories.UsersRepositoryFactory;

public class Main {
    public static void main(String[] args) {
        UsersRepository repository = UsersRepositoryFactory.onJdbc();

        User user = new User("Василий", "Уткин", "emailUtkin@gmail.com", "qwerty000012");

        repository.save(user);

        System.out.println(user);
    }
}