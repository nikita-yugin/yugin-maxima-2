package Application.validators;

import Application.validators.exceptions.EmailValidatorException;

public interface EmailValidator {
    void validate(String email) throws EmailValidatorException;
}
