package Application.services;

import Application.services.exceptions.AuthenticationException;

public interface UsersService {
    /**
     * Регистрирует пользователя в системе
     * @param firstName - имя пользователя
     * @param lastName - фамилия пользователя
     * @param email - Email пользователя
     * @param password - пароль пользователя
     */
    void signUp(String firstName, String lastName, String email, String password);


    /**
     * Проверяет, есть ли такой пользователь в базе данных с такими данными
     * @param email - Email пользователя
     * @param password - пароль пользователя
     * @throws AuthenticationException - в случае если email|пароль не верные
     */
    void signIn(String email, String password) throws AuthenticationException;
}
