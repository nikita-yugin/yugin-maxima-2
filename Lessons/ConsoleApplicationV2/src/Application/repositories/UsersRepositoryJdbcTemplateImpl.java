package Application.repositories;

import Application.models.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_INSERT_INTO_ACCOUNT = "insert into account(first_name, last_name, email, password)" +
            "values (?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL = "select * from account " +
            "where email = ? limit 1";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from account " +
            "where id = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_HAS_LICENCE = "select * from account " +
            "where has_licence = ?";

    //language=SQL
    private static final String SQL_UPDATE_ACCOUNT_BY_ID = "update account set first_name = ?, " +
            "last_name = ?, email = ?, password = ? where id = ?";

    private final JdbcTemplate jdbcTemplate;

    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<User> usersRowMapper = (RowMapper<User>) (row, rowNumber) -> new User(
            row.getLong("id"),
            row.getString("first_name"),
            row.getString("last_name"),
            row.getString("email"),
            row.getString("password"));

    @Override
    public void save(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_ACCOUNT,
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());

            return statement;
        }, keyHolder);

        Long generatedId = ((Integer) keyHolder.getKeys().get("id")).longValue();
        user.setId(generatedId);
    }

    @Override
    public Optional<User> findOneByEmail(String email) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL, usersRowMapper, email));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, usersRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<User> findAllByHasLicence(boolean hasLicence) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_HAS_LICENCE, usersRowMapper, hasLicence);
    }

    @Override
    public void update(User user) {
        jdbcTemplate.update(
                SQL_UPDATE_ACCOUNT_BY_ID,
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                user.getId());
    }
}
