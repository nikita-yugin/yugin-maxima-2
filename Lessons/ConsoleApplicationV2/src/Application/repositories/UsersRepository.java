package Application.repositories;

import Application.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {

    void save(User user);

    Optional<User> findOneByEmail(String email);

    Optional<User> findById(Long id);

    List<User> findAllByHasLicence(boolean hasLicence);

    void update(User user);
}
