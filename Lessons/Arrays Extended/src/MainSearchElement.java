import java.util.Scanner;

public class MainSearchElement {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int size = scanner.nextInt();
        int[] array = new int[size];

        System.out.println("Заполните массив: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Введите элемент, который хотите найти: ");
        int element = scanner.nextInt();
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (element == array[i]) {
                index = i;
            }
        }

        if (index != -1) {
            System.out.println("Число, которое вы ищете находится под индексом: " + index);
        } else {
            System.out.println("Элемент не найден");
        }
    }
}
