import java.util.Arrays;
import java.util.Scanner;

public class MainSelectionSort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
            int[] array = {65, 23, 1, 15, 97, -14, 20, 66, 112, -35};
        int min;
        int indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;

            System.out.println(Arrays.toString(array));
        }
    }
}
