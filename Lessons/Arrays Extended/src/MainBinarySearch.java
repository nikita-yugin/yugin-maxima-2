import java.util.Scanner;

public class MainBinarySearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите элемент: ");
        int element = scanner.nextInt();
        boolean hasElement = false;

        int[] array = {-35, -14, 1, 15, 20, 23, 65, 66, 97, 112};

        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;
        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
            } else if (element > array[middle]) {
                left = middle + 1;
            } else {
                hasElement = true;
                break;
            }
            middle = left + (right - left) / 2;
        }

        System.out.println(hasElement);
    }
}
