public class Main {

    // математическая функция  y = x^2
    public static double f(double x) {
        return x * x;
    }

    // МЕТОД ПРЯМОУГОЛЬНИКОВ
    public static double integralByRectangle(double a, double b, int n) {
        // находим ширину прямоугольника
        double width = (b - a) / n;
        double sum = 0;

        // цикл в котором мы находим сумму площадей всех прямоугольников
        for (double x = a; x <= b; x += width) {
            // высота прямоугольника
            double height = f(x);
            // площадь прямоугольника
            double currentRectangleArea = height * width;
            // суммируем площади всех прямоугольников
            sum += currentRectangleArea;
        }

        return sum;
    }

    // МЕТОД СИМПОСНА
    public static double integralBySimpson(double a, double b, int n) {
        // находим высоту
        double h = (b - a) / n;
        double sum = 0;

        // цикл в котором мы находим сумму по формуле
        for (double x = a; x <= b; x += h) {
            double k = (x + (x + h)) / 2;
            // складываем по формуле
            sum += (h / 3) * (f(x) + 4 * f(k) + f(x + h)) / 2;
        }

        return sum;
    }

    public static void main(String[] args) {
        // массив с заранее подготовленными разбиениями
        int[] ns = {10, 100, 1000, 10_000, 100_000, 200_000, 300_000, 500_000, 1_000_000, 2_000_000};
        // массив с результатами разбиений для метода прямоугольников
        double[] ysForRect = new double[ns.length];
        // с результатами разбиений для метода Симпосона
        double[] ysForSimpson = new double[ns.length];
        int from = 0;
        int to = 10;
        double realValue = 333.33333333;

        // цикл считающий интегралы для всех разбиений на заданном диапазоне по методу прямоугольника
        for (int i = 0; i < ns.length; i++) {
            ysForRect[i] = integralByRectangle(from, to, ns[i]);
        }

        // цикл считающий интегралы для всех разбиений на заданном диапазоне по методу Симпсона
        for (int i = 0; i < ns.length; i++) {
            ysForSimpson[i] = integralBySimpson(from, to, ns[i]);
        }

        System.out.println("Метод прямоугольников:");

        // Выводим результаты метода прямоугольников
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("| X = %10d | Y = %10.4f | EPS = %10.5f |\n", ns[i], ysForRect[i], Math.abs(ysForRect[i] - realValue));
        }

        System.out.println("Метод Симпсона:");

        // выводим результат метода Симпсона
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("| X = %10d | Y = %10.4f | EPS = %10.5f | \n", ns[i], ysForSimpson[i], Math.abs(ysForSimpson[i] - realValue));
        }
    }

}
