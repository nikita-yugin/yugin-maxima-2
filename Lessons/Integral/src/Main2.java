public class Main2 {

    public static double f(double x) {
        return Math.sin(x) * 2;
    }

    public static double integralByRectangle(double a, double b, int n) {
        double width = (b - a) / n;
        double sum = 0;

        for (double x = a; x <= b; x += width) {
            double height = f(x);
            double currentRectangleArea = height * width;
            sum += currentRectangleArea;
        }

        return sum;
    }

    public static void main(String[] args) {
        int[] ns = {10, 100, 1000, 10_000, 100_000, 200_000, 300_000, 500_000, 1_000_000, 2_000_000};
        double[] ys = new double[ns.length];
        int from = 0;
        int to = 10;
        double realValue = 3.67814305815290;

        for (int i = 0; i < ns.length; i++) {
            ys[i] = integralByRectangle(from, to, ns[i]);
        }

        for (int i = 0; i < ns.length; i++) {
            System.out.printf("| X = %10d | Y = %10.4f | EPS = %10.5f |\n", ns[i], ys[i], Math.abs(ys[i] - realValue));
        }
    }

}
