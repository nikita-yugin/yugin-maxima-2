public class MainIntegral {

    // математическая функция y = x^2
    public static double f(double x) {
        return x * x;
    }

    // МЕТОД ПРЯМОУГОЛЬНИКОВ
    public static double integralByRectangle(double a, double b, int n) {
        // находим ширину прямоугольников
        double width = (b - a) / n;
        double sum = 0;

        // находим сумму площадей прямоугольников
        for (double x = a; x <= b; x += width) {
            // находим высоту
            double height = f(x);
            // находим площадь прямоугольника
            double currentRectangleArea = height * width;
            // суммируем полученные площади
            sum += currentRectangleArea;
        }

        return sum;
    }

    // МЕТОД СИМПСОНА
    public static double integralBySimpson(double a, double b, int n) {
        double height = (b - a) / n;
        double sum = 0;

        for (double x = a; x <= b; x += height) {
            double k = (x + (x + height)) / 2;
            sum += (height / 3) * (f(x) + 4 * f(k) + f(x + height)) / 2;
        }

        return sum;
    }

    public static void main(String[] args) {
        // массив с заготовленными разбиениями
        int[] ns = {10, 100, 1000, 10_000, 100_000, 200_000, 300_000, 500_000, 1_000_000, 2_000_000};
        // массив с результатами разбиений для метода прямоугольника
        double[] ysFromRectangle = new double[ns.length];
        // массив с результатами разбиений для метода Симпсона
        double[] ysFromSimpson = new double[ns.length];

        int from = 0;
        int to = 10;
        double realValue = 333.33333333;

        // считаем интегралы для всех разбиений по методу прямоугольника
        for (int i = 0; i < ns.length; i++) {
            ysFromRectangle[i] = integralByRectangle(from, to, ns[i]);
        }

        // считаем интегралы для всех разбиений по методу Симпсона
        for (int i = 0; i < ns.length; i++) {
            ysFromSimpson[i] = integralBySimpson(from, to, ns[i]);
        }

        // выводим на экран результаты вычислений по методу прямоугольника
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("| X = %10d | Y = %10.4f | EPS = %10.5f |\n", ns[i], ysFromRectangle[i], Math.abs(ysFromRectangle[i] - realValue));
        }

        System.out.println();

        // выводим на экран результаты вычислений по методу Симпсона
        for (int i = 0; i < ns.length; i++) {
            System.out.printf("| X = %10d | Y = %10.4f | EPS = %10.5f |\n", ns[i], ysFromSimpson[i], Math.abs(ysFromSimpson[i] - realValue));
        }
        
    }

}
