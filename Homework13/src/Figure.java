public abstract class Figure implements SpaceObject {
    protected double x;
    protected double y;
    protected double z;

    public Figure(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void move(double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }
}
