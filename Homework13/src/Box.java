public class Box extends Figure implements Shape, SpaceObject {
    private double length;
    private double height;
    private double width;

    public Box(double x, double y, double z, double length, double height, double width) {
        super(x, y, z);
        this.length = length;
        this.height = height;
        this.width = width;
    }


    @Override
    public void scale(double value) {
        this.length *= value;
        this.height *= value;
        this.width *= value;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }
}
