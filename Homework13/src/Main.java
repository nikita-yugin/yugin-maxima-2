

public class Main {
    public static void main(String[] args) {
        Box box = new Box(10, 11, 17, 10, 10, 10);
        Sphere sphere = new Sphere(1, 15, 20, 5);
        Point point = new Point(5, 11, 45);

        box.move(0, 0, 0);
        sphere.move(6, 4, 11);
        point.move(0, 0, 33);

        box.scale(2);
        sphere.scale(10);
    }
}
