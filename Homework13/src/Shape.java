public interface Shape {
    void scale(double value);
}
