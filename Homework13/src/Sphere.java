public class Sphere extends Figure implements Shape, SpaceObject {
    private double radius;

    public Sphere(double x, double y, double z, double radius) {
        super(x, y, z);
        this.radius = radius;
    }

    @Override
    public void scale(double value) {
        radius *= value;
    }
}
