package com.company;

public class MainDigitsSum {

    public static int sumOfDigits(int number) {
        int digitsSum = 0;

        if (number < 10) {
            return number;
        } else {
            return number % 10 + sumOfDigits(number / 10);
        }
    }

    public static void main(String[] args) {
        System.out.println(sumOfDigits(17854));
    }
}
