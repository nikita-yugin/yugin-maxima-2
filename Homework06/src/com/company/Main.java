package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);
        int[] array = {65, 23, 1, 15, 97, -14, 20, 66, 112, -35};
        selectionSort(array);
        System.out.println(Arrays.toString(array));

        int element = scanner.nextInt();
        System.out.println(search(array, element));
    }

    public static void selectionSort(int[] array) {
        int min = 0;
        int indexOfMin = 0;
        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
    }

    public static boolean search(int[] array, int element) {
        boolean hasElement = false;

        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;

        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
            } else if (element > array[middle]) {
                left = middle + 1;
            } else {
                hasElement = true;
                break;
            }
            middle = left + (right - left) / 2;
        }

        return hasElement;
    }
}
