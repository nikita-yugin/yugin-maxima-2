import models.Car;
import repositories.CarRepository;
import repositories.CarRepositoryImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        CarRepository repository = new CarRepositoryImpl("input.txt");

        System.out.println(repository.findAll());

        List<Car> byColorOrMileage = repository.findByColorOrMileage("Black", 0);

        byColorOrMileage
                .stream()
                .map(Car::getNumber)
                .forEach(System.out::println);

        System.out.println("Кол-во машин в диапазоне от 700 до 800 - " + repository.findByPriceFrom700To800());

        System.out.println("Цвет машины с минимальным пробегом: " + repository.findCarMinPrice());

        System.out.println("Средняя цена Camry: " + repository.averagePrice());
    }
}