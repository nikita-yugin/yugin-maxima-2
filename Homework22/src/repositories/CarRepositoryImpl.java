package repositories;

import models.Car;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarRepositoryImpl implements CarRepository {

    private String filename;

    public CarRepositoryImpl(String filename) {
        this.filename = filename;
    }

    private static final Function<String, Car> toCarMapper = string -> {
        String[] parsedLine = string.split("\\|");

        return new Car(
                parsedLine[0],
                parsedLine[1],
                parsedLine[2],
                Integer.parseInt(parsedLine[3]),
                Integer.parseInt(parsedLine[4]));
    };

    @Override
    public List<Car> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            return reader.lines()
                    .map(toCarMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Car> findByColorOrMileage(String color, Integer mileage) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            return reader.lines()
                    .map(toCarMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage().equals(mileage))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Long findByPriceFrom700To800() {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            return reader
                    .lines()
                    .map(toCarMapper)
                    .distinct()
                    .filter(car -> car.getPrice() > 700 && car.getPrice() < 800)
                    .count();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String findCarMinPrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            return reader
                    .lines()
                    .map(toCarMapper)
                    .min(Comparator.comparingInt(Car::getPrice))
                    .map(Car::getColor)
                    .get();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Double averagePrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            return reader
                    .lines()
                    .map(toCarMapper)
                    .mapToInt(Car::getPrice)
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
