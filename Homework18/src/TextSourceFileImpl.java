import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TextSourceFileImpl implements TextSource {

    private final String filename;

    public TextSourceFileImpl(String filename) {
        this.filename = filename;
    }

    @Override
    public String getText() {
        try {
            FileReader fileReader = new FileReader(filename);
            char[] characters = new char[1024];
            fileReader.read(characters);
            return String.valueOf(characters);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e)               ;
        }
    }
}
