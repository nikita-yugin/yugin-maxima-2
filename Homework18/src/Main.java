import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        TextSource textSource = new TextSourceFileImpl("input.txt");

        String text = textSource.getText();

        Map<String, Integer> words = getStatistics(text);

        System.out.println(words);
    }

    private static Map<String, Integer> getStatistics(String text) {
        Map<String, Integer> words = new HashMap<>();
        String[] wordsArray = text.split("\\w+");

        for (String word : wordsArray) {
            if (words.containsKey(word)) {
                words.put(word, words.get(word) + 1);
            } else {
                words.put(word, 1);
            }
        }

        return words;
    }
}
