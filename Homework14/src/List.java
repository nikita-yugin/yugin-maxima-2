public interface List<T> {
    public void add(T element);

    T get(int index);

    int size();

    void removeAt(int index);

    void remove(T element);

    void add(int index, T element);
}
