import java.util.Objects;

public class LinkedList<E> implements example3.List<E> {

    private static class Node<V> {
        V value;
        Node<V> next;

        Node(V value) {
            this.value = value;
        }
    }

    private Node<E> first;
    private Node<E> last;
    private int size = 0;

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);

        if (isEmpty()) {
            first = newNode;
        } else {
            last.next = newNode;
        }

        last = newNode;

        size++;
    }

    @Override
    public E get(int index) {
        Node<E> current = first;

        int i = 0;

        while (i < size) {
            if (index == i) {
                return current.value;
            } else {
                current = current.next;
                i++;
            }
        }

        return null;
    }

    @Override
    public int size() {
        return size;
    }



    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void removeAt(int index) {
        Node<E> previousRemove = first;

        if (index == 0) {
            first = first.next;
            size--;
            return;
        }

        for (int i = 0; i < index - 1; i++) {
            previousRemove = previousRemove.next;
        }

        if (index == (size - 1)) {
            last = previousRemove;
        }

        Node<E> forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    @Override
    public void remove(E element) {
        if (first.value == element) {
            removeAt(0);
            return;
        }

        Node<E> prev = first;
        Node<E> current = first.next;
        while (prev.next != null) {
            if (Objects.equals(current.value, element)) {
                if (current == last) {
                    last = prev;
                    prev.next = null;
                } else {
                    prev.next = current.next;
                }

                size--;
            }

            prev = prev.next;
            current = current.next;
        }
    }

    @Override
    public void add(int index, E element) {
        Node<E> newNode = new Node<>(element);

        if (isEmpty()) {
            first = newNode;
        } else if (index == size - 1) {
            last.next = newNode;
            last = newNode;
        } else {
            Node<E> currentNode = first;
            for (int i = 0; i < index - 1; i++) {
                currentNode = currentNode.next;
            }

            newNode.next = currentNode.next;
            currentNode.next = newNode;
        }

        size++;
    }
}
