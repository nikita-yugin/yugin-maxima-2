public abstract class Figure {
    private int x;
    private int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract double getPerimeter();

    public void move(int newX, int newY) {
        this.x = newX;
        this.y = newY;
    }
}
