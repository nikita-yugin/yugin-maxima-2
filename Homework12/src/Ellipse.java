public class Ellipse extends Figure {
    protected int diagonalA;
    protected int diagonalB;

    public Ellipse(int diagonalA, int diagonalB, int x, int y) {
        super(x, y);
        this.diagonalA = diagonalA;
        this.diagonalB = diagonalB;
    }

    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((diagonalA * diagonalA) + (diagonalB * diagonalB) / 2);
    }
}
