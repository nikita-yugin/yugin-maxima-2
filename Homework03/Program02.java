import java.util.Scanner;

class Program02 {
	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int previousNum = 0;
        int number = scanner.nextInt();
        int nextNum = scanner.nextInt();
        int localMins = 0;

        System.out.println("Введите числа: ")
        while (nextNum != -1) {
        	if (number < previousNum && number < nextNum) {
        		localMins++;
        	}

        	previousNum = number;
        	number = nextNum;
        	nextNum = scanner.nextInt();
        }

        System.out.println("Число локальных минимумов: " + localMins);
	}
}
