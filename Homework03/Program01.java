import java.util.Scanner;

class Program01 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		int digitsSum = 0;
		int minNumber = 0;
		int min = number;

		while (number != -1) {
			if (number < min) {
				min = number;
			}
			number = scanner.nextInt();
		}

		minNumber = min;

		while(min != 0) {
			int lastDigit = min % 10;
			min = min / 10;
			digitsSum += lastDigit;
		}

		System.out.println("Минимальное число последовательности: " + minNumber + ", его сумма: " + digitsSum);
	}	
}
