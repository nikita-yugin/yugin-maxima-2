drop table if exists food_products;

create table food_products
(
    id serial primary key,
    product_name char(20),
    price int default 1 not null check ( price > 0 ),
    self_life int default 1 not null check ( self_life > 0 and self_life < 11 ),
    delivery_date date default '2020-10-02' not null,
    supplier_name char(20) not null
);

insert into food_products(product_name, price, self_life, delivery_date, supplier_name) values ('Ford Mustang', 2500000, 3, '2022-02-05', 'Ford Russia');
insert into food_products(product_name, price, self_life, supplier_name) values ('Range Rover', 7000000, 3, 'Land Rover Rus');
insert into food_products(product_name, price, self_life, delivery_date, supplier_name) values ('Audi RS6', 4500000, 3, '2019-12-29', 'Audi Rus');
insert into food_products(product_name, price, self_life, delivery_date, supplier_name) values ('VW Golf', 2000000, 3, '2021-03-17', 'VW Rus');
insert into food_products(product_name, price, self_life, supplier_name) values ('Mazda 6', 2250000, 3, 'Mazda Rus');

-- получить товары которые дороже 2.500.000 руб.
select * from food_products where price > 2500000;

-- получить товары поставленные ранее 2020-02-02
select * from food_products where delivery_date < '2020-02-02';

-- получить названия всех поставщиков
select supplier_name from food_products;